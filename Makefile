.PHONY: all compile clean test echo

all: compile

compile:
	dune build

demo:
	./_build/install/default/bin/demo

clean:
	dune clean

