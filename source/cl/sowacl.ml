open Cmdliner

let copts = Term.const ()

module Glue = struct

  let sh_parse sh = match Sowa.Parser.global_parse_string sh with
    | None -> Error (`Msg "cannot parse")
    | Some x -> Ok x

  let sh_fmt fmt sh = Format.fprintf fmt "%s" (Sowa.Logic.SymbolicHeap.str sh)

  let sh_converter =
    let docv = "SYMBOLIC_HEAP" in
    Cmdliner.Arg.conv ~docv (sh_parse, sh_fmt)

  let sh_arg n =
    let open Arg in
    let docv = "SYMBOLIC_HEAP" in 
    required & pos n (some sh_converter) None & info [] ~docv

end

module Common = struct

  let verify_flag = 
    let open Arg in
    value (flag (info ["verify"]))

  let lhs_arg = 
    let open Arg in
    let docv = "LHS_SYMBOLIC_HEAP" in 
    opt (some Glue.sh_converter) None (info ["lhs"] ~docv)

  let rhs_arg = 
    let open Arg in
    let docv = "RHS_SYMBOLIC_HEAP" in 
    opt (some Glue.sh_converter) None (info ["rhs"] ~docv)

end

module ParseCmd = struct

  let impl sh = 
      Printf.printf "show:\n%s\n\nstr:\n%s\n" 
        (Sowa.Logic.SymbolicHeap.show sh)
        (Sowa.Logic.SymbolicHeap.str sh);
      0


  let cmd =
    let doc = "Parse SymbolicHeap and print it" in
    let open Term in 
    let f = Term.const impl in
    f $ (Glue.sh_arg 0),
    info "parse" ~doc
end

module ProveCmd = struct

  let impl lhs rhs = 
      Printf.printf "LHS:\n%s\n" 
        (Sowa.Logic.SymbolicHeap.str lhs);
      Printf.printf "RHS:\n%s\n" 
        (Sowa.Logic.SymbolicHeap.str rhs);
      let result = Sowa.Prover.prove (lhs, rhs) in
      Printf.printf "Result: %b\n" result;
      if result then
        0
      else 
        1

  let cmd =
    let doc = "Prove given entailment" in
    let open Term in 
    let f = Term.const impl in
    f $ (Arg.required Common.lhs_arg) $ (Arg.required Common.rhs_arg),
    info "prove" ~doc

end

module NormCmd = struct

  let impl sh = 
      Printf.printf "SH:\n%s\n" 
        (Sowa.Logic.SymbolicHeap.str sh);
      let result = Sowa.Normalizer.normalize_sh sh in
      Printf.printf "Normalized: %s\n" (Sowa.Logic.SymbolicHeap.str result);
      0


  let cmd =
    let doc = "Parse SymbolicHeap and print it" in
    let open Term in 
    let f = Term.const impl in
    f $ (Glue.sh_arg 0),
    info "norm" ~doc
end

module NormPairCmd = struct

  let impl lhs rhs = 
      Printf.printf "LHS:\n%s\n" 
        (Sowa.Logic.SymbolicHeap.str lhs);
      Printf.printf "RHS:\n%s\n" 
        (Sowa.Logic.SymbolicHeap.str rhs);
      let (lhs, rhs)= Sowa.Normalizer.normalize_pair (lhs, rhs) in
      Printf.printf "Result:\n";
      Printf.printf "LHS:\n%s\n" 
        (Sowa.Logic.SymbolicHeap.str lhs);
      Printf.printf "RHS:\n%s\n" 
        (Sowa.Logic.SymbolicHeap.str rhs);
      0


  let cmd =
    let doc = "Normalize pair" in
    let open Term in 
    let f = Term.const impl in
    f $ (Glue.sh_arg 0) $ (Glue.sh_arg 1),
    info "normpair" ~doc
end

module FrameCmd = struct

  let impl lhs rhs = 
      Printf.printf "LHS:\n%s\n" 
        (Sowa.Logic.SymbolicHeap.str lhs);
      Printf.printf "RHS:\n%s\n" 
        (Sowa.Logic.SymbolicHeap.str rhs);
      let result = Sowa.Prover.infer_frame (lhs, rhs) in
      match result with
      | None ->
        Printf.printf "Cannot infer frame\n";
        1
      | Some sh ->
        Printf.printf "Frame: %s\n" (Sowa.Logic.SymbolicHeap.str sh);
        0


  let cmd =
    let doc = "Frame inference" in
    let open Term in 
    let f = Term.const impl in
    f $ (Arg.required Common.lhs_arg) $ (Arg.required Common.rhs_arg),
    info "frame" ~doc

end

module AbductCmd = struct

  let impl lhs rhs verify = 
      Printf.printf "LHS:\n%s\n" 
        (Sowa.Logic.SymbolicHeap.str lhs);
      Printf.printf "RHS:\n%s\n" 
        (Sowa.Logic.SymbolicHeap.str rhs);
      let result = Sowa.Biabduction.abduct (lhs, rhs) in
      match result with
      | None ->
        Printf.printf "Cannot abduct antiframe\n";
        1
      | Some antiframe ->
        Printf.printf "AntiFrame: %s\n" (Sowa.Logic.SymbolicHeap.str antiframe);
        if not verify then
          0
        else begin
          Printf.printf "====> Verification requested\n";
          let lhs = Sowa.Logic.SymbolicHeap.join lhs antiframe in 
          Printf.printf "LHS:\n%s\n" 
            (Sowa.Logic.SymbolicHeap.str lhs);
          Printf.printf "RHS:\n%s\n" 
            (Sowa.Logic.SymbolicHeap.str rhs);
          if Sowa.Prover.prove (lhs, rhs) then begin
            Printf.printf "Verified\n";
            0
          end else begin
            Printf.printf "Verification failed\n";
            1
          end
        end

  let cmd =
    let doc = "Abduct anti-frame for given entailment" in
    let open Term in 
    let f = Term.const impl in
    f $ (Arg.required Common.lhs_arg) $ (Arg.required Common.rhs_arg) $ (Common.verify_flag),
    info "abduct" ~doc

end
module BiAbductCmd = struct

  let impl lhs rhs verify = 
      Printf.printf "LHS:\n%s\n" 
        (Sowa.Logic.SymbolicHeap.str lhs);
      Printf.printf "RHS:\n%s\n" 
        (Sowa.Logic.SymbolicHeap.str rhs);
      let result = Sowa.Biabduction.biabduct (lhs, rhs) in
      match result with
      | None ->
        Printf.printf "Cannot biabduct\n";
        1
      | Some (antiframe, frame) ->
        Printf.printf "Frame: %s\n" (Sowa.Logic.SymbolicHeap.str frame);
        Printf.printf "AntiFrame: %s\n" (Sowa.Logic.SymbolicHeap.str antiframe);
        if not verify then
          0
        else begin
          Printf.printf "====> Verification requested\n";
          let lhs = Sowa.Logic.SymbolicHeap.join lhs antiframe in 
          let rhs = Sowa.Logic.SymbolicHeap.join rhs frame in 
          Printf.printf "LHS:\n%s\n" 
            (Sowa.Logic.SymbolicHeap.str lhs);
          Printf.printf "RHS:\n%s\n" 
            (Sowa.Logic.SymbolicHeap.str rhs);
          if Sowa.Prover.prove (lhs, rhs) then begin
            Printf.printf "Verified\n";
            0
          end else begin
            Printf.printf "Verification failed\n";
            1
          end
        end



  let cmd =
    let doc = "Abduct anti-frame for given entailment" in
    let open Term in 
    let f = Term.const impl in
    f $ (Arg.required Common.lhs_arg) $ (Arg.required Common.rhs_arg) $ (Common.verify_flag),
    info "biabduct" ~doc

end



module DefaultCmd = struct 

  let cmd =
    let doc = "SOWA Prover Command Line" in
    Term.(ret (const (fun _ -> `Help (`Pager, None)) $ copts)),
    Term.info "sowacl"  ~doc 

end

let cmds =
  [ ParseCmd.cmd
  ; ProveCmd.cmd
  ; FrameCmd.cmd
  ; AbductCmd.cmd
  ; BiAbductCmd.cmd
  ; NormCmd.cmd
  ; NormPairCmd.cmd
  ]

let _ = Term.exit_status (Term.eval_choice DefaultCmd.cmd cmds)