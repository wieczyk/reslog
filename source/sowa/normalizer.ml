open Logic

let debugf fmt = Lib.Log.make_debug_printf __MODULE__ fmt

module Rewriter = struct

  type goal = SymbolicHeap.t 

  type 'm rule_matcher = goal -> 'm Choice.t

  type 'm rule_executor =  goal -> 'm -> goal option

  type 'm rule_impl = 
    { rmatch : 'm rule_matcher
    ; rexec : 'm rule_executor
    }

  type rule_descr = 
    | Rule : { name: string ; impl: 'm rule_impl } -> rule_descr

  let mk_rule name rmatch rexec = Rule {name; impl={rmatch; rexec}}

  let rewrite rules start = 
    let counter = ref 0 in

    let rec apply goal tag = function
      | [] ->
        None
      
      | (Rule {name; impl={rmatch; rexec}})::rs ->
        debugf "- trying %s" name;
        let module Let_syntax = Lib.Extseq.Let_syntax in 
        let cc = Choice.(
          let%bind m = rmatch goal in 
          return (rexec goal m)
        ) in
        match Choice.run_one cc with
        | Some (Some x) -> Some x
        | _ ->
          apply goal tag rs
    in

    let rec loop goal =
      let tag = !counter in
      incr counter;
      match apply goal tag rules with
      | Some x ->
        loop x
      | None ->
        goal
    in

    loop start
end


module Rules = struct
  open Choice
  open Rewriter
  module Let_syntax = Lib.Extseq.Let_syntax

  module Common = struct

      let product_without_diag t =
        let p = product t t in
        let%bind (a,b) = p in
        let%bind _ = guard (a != b) in
        return (a,b)

  end
  open Common 

  module DupTrue = struct
    let matcher sh = 
      let%bind s1, s2 = product_without_diag @@ SymbolicHeap.any_spatial sh in
      match s1, s2 with
      | Spatial.S_True, Spatial.S_True ->
        return s1
      | _ ->
        fail

    let exec sh s =
      Some (SymbolicHeap.remove_spatial sh s)

    let rule = mk_rule "N_DupTrue" matcher exec
  end

  let rules = 
    [ DupTrue.rule
    ]
end

module VariableNormalizer = struct

  let assign_newvar offset htable var = match var with
    | Var.VAR_Logic (LogVar.Normal _) ->
      if not (VarHashtbl.mem htable var) then begin
        let i' = !offset in
        incr offset;
        let var' = (Expr.E_Var (Var.VAR_Logic (LogVar.Normal i'))) in
        VarHashtbl.replace htable var var';
      end;
      true
    | _ ->
      true


  let compute_sb offset sh =
    let fv = SymbolicHeap.fv sh in 
    let htable = VarHashtbl.create 513 in
    Choice.iter fv (assign_newvar offset htable);
    htable

  let normalize offset sh = 
    let counter = ref offset in
    let sb = compute_sb counter sh in
    let sh = SymbolicHeap.subst_htable sh sb in
    (sh, !counter)

end

type norm_result = NormInfo of
  { next_variable_index : int
  ; sh : SymbolicHeap.t
  }

let rule_based_normalize = Rewriter.rewrite Rules.rules

let normalize offset sh =
  let sh = rule_based_normalize sh in
  let (sh, next_variable_index) = VariableNormalizer.normalize offset sh in
  NormInfo {next_variable_index; sh}

let normalize_sh ?(offset=0) sh = 
  match normalize offset sh with
  | NormInfo {sh; _} ->
    sh

let normalize_pair (lhs, rhs) =
  match normalize 0 lhs with
  | NormInfo {sh=lhs; next_variable_index} ->
    let rhs = normalize_sh ~offset:next_variable_index rhs in 
    (lhs, rhs)