open Logic

let debugf fmt = Lib.Log.make_debug_printf __MODULE__ fmt

module RecProofSearch = struct

  type goal = SymbolicHeap.t * SymbolicHeap.t

  type 'm rule_matcher = goal -> 'm Choice.t

  type recursor = (goal -> SymbolicHeap.t option)

  type 'm rule_executor = recursor -> goal -> 'm -> SymbolicHeap.t option

  type 'm rule_impl = 
    { rmatch : 'm rule_matcher
    ; rexec : 'm rule_executor
    }

  type rule_descr = 
    | Rule : { name: string ; impl: 'm rule_impl } -> rule_descr

  let mk_rule name rmatch rexec = Rule {name; impl={rmatch; rexec}}


  let prove rules start_goal =
    let counter = ref 0 in

    let rec recursor goal = 
      let tag = !counter in
      incr counter;
      debugf "Goal %u" tag;
      debugf "     - LHS: %s" (SymbolicHeap.str (fst goal));
      debugf "     - RHS: %s" (SymbolicHeap.str (snd goal));
      apply goal tag rules

    and apply goal tag = function
      | [] ->
        None
      | (Rule {name; impl={rmatch; rexec}})::rs ->
        debugf "- trying %s" name;
        let module Let_syntax = Lib.Extseq.Let_syntax in 
        let cc = Choice.(
          let%bind m = rmatch goal in 
          return (rexec recursor goal m)
        ) in
        match Choice.run_one cc with
        | Some (Some x) -> Some x
        | _ ->
          apply goal tag rs
      in

      recursor start_goal
end

module AbductionRules = struct
  open RecProofSearch

  let list_len = List.length

  open Choice

  module Let_syntax = Lib.Extseq.Let_syntax

  module Common = struct

    let length t = list_len (Choice.run_all t)

    let unpack_pto1 = function
      | Spatial.S_Pto1 (e1, fld, e2) ->
        return (e1, fld, e2)
      | _ ->
        fail

  end
  open Common

  module BaseTrue = struct
    (*
      when RHS has only True in spatial 
      ---------------------------------
      D * [RHS] |- RHS
     *)

     let matcher (_, rhs) =
      let%bind _ = guard @@ (length (SymbolicHeap.any_spatial rhs) = 1) in 
      match%bind SymbolicHeap.any_spatial rhs with
      | Spatial.S_True ->
        return ()
      | _ ->
        fail

    let exec _ (_, rhs) () =
      Some (SymbolicHeap.only_pure rhs)

    let rule = mk_rule "A_BaseTrue" matcher exec
  end

  module BaseEmp = struct
    (*
      when RHS  and LHS has empty spatial
      ---------------------------------
      D * [RHS] |- RHS
     *)

    let matcher (lhs, rhs) =
      let%bind _ = guard @@ SymbolicHeap.is_empty_spatial lhs in 
      let%bind _ = guard @@ SymbolicHeap.is_empty_spatial rhs in 
      return ()

    let exec _ (_, rhs) () =
      Some rhs

    let rule = mk_rule "A_BaseEmp" matcher exec

  end

  module Pto = struct
    (*
      LHS /\ E1 == E2 * [M] |- 
      ------------------------------------------------------------
      LHS ** E.f |-> E2 * [M'] |- <exists xs ys> RHS ** E.f |-> E1

      M' = M ** E1 == E2

      FreeLVAR(E2) * ys = 0
     *)

    let matcher (lhs, rhs) =
      let%bind l = SymbolicHeap.any_spatial lhs in
      let%bind (e', f', e2) = unpack_pto1 l in 

      let%bind r = SymbolicHeap.any_spatial rhs in
      let%bind (e, f, e1) = unpack_pto1 r in 

      let%bind _ = guard @@ Expr.equal e e' in
      let%bind _ = guard @@ Basic.equal_field f f' in

      let eq = Pure.P_Eq (e1, e2) in
      return (l, r, eq)

  let exec reccall (lhs, rhs) (l, r, eq) =
      let module Let_syntax = Lib.Extseq.Option_Let_syntax in 
      let lhs = SymbolicHeap.insert_pure lhs eq in
      let lhs = SymbolicHeap.remove_spatial lhs l in 
      let rhs = SymbolicHeap.remove_spatial rhs r in 
      let%bind m = reccall (lhs, rhs) in 
      let m = SymbolicHeap.insert_pure m eq in 
      Some m

    let rule = mk_rule "A_Pto" matcher exec
  end

  module StarIntro = struct
    (*
      Eliminates the same spatial predicate from both sides.
      This rule should be executed when all other, except True-rules, failed. 

      LHS |- RHS
      ----------------------
      LHS ** S |- RHS ** S
    *)

    let matcher (lhs, rhs) =
      let%bind l = SymbolicHeap.any_spatial rhs in
      let%bind r = SymbolicHeap.any_spatial lhs in 
      let%bind _ = guard (Spatial.equal l r) in
      return l

    let exec reccall (lhs, rhs) s =
      let module Let_syntax = Lib.Extseq.Option_Let_syntax in 
      let lhs = SymbolicHeap.remove_spatial lhs s in
      let rhs = SymbolicHeap.remove_spatial rhs s in
      let%bind m = reccall (lhs, rhs) in
      Some m

    let rule = mk_rule "A_StarIntro" matcher exec

  end

  module Missing = struct

    let matcher (lhs, rhs) =
      let%bind s = SymbolicHeap.any_spatial rhs in
      let%bind _ = guard (Spatial.S_True <> s) in
      let lhs = SymbolicHeap.insert_spatial lhs s in 
      let%bind _ = guard (Prover.consistent lhs) in
      return s

    let exec (reccall : recursor) (lhs, rhs) s =
      let module Let_syntax = Lib.Extseq.Option_Let_syntax in 
      let rhs = SymbolicHeap.remove_spatial rhs s in
      let%bind m = reccall (lhs, rhs) in
      let m = SymbolicHeap.insert_spatial m s in 
      Some m

    let rule = mk_rule "A_Missing" matcher exec

  end

let rules =
  [ BaseEmp.rule
  ; BaseTrue.rule
  ; Pto.rule
  ; StarIntro.rule
  ; Missing.rule
  ]

end

let abduct goal =
  debugf "Calling abducer";
  let r = RecProofSearch.prove AbductionRules.rules goal in
  if Lib.Utils.is_some r then
    debugf "Abduction success!"
  else
    debugf "Abduction failed!";
  r
  

let biabduct (lhs, rhs) =
  debugf "Calling bi-abducer";
  let goal_abd_opt = (lhs, SymbolicHeap.insert_spatial rhs Spatial.S_True) in
  match abduct goal_abd_opt with
  | None -> 
    debugf "Bi-abduction failed during abduction";
    None
  | Some antiframe ->
    let lhs = SymbolicHeap.join lhs antiframe in
    match Prover.infer_frame (lhs, rhs) with
    | None ->
      debugf "Bi-abduction failed during frame-inference";
      None
    | Some frame ->
      debugf "Bi-abduction success";
      Some (antiframe, frame)
