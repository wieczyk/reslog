open Ppx_compare_lib.Builtin

module Basic = struct

  type typename =
    | ProgtypeName of string
    [@@deriving compare, show]

  type field =
    | Field of string
    [@@deriving compare, show]

  let str_typename (ProgtypeName t) = t

  let str_field = function
    | Field f -> f

  let equal_field = [%compare.equal: field]
end

module ProgVar = struct

  type t =
    | Progvar of string
    [@@deriving compare, show]

  let str (Progvar t) = "%" ^ t

  let equal = [%compare.equal: t]
end

module LogVar = struct

  type t =
    | Normal of int
    | Special of int
    [@@deriving compare, show]

  let equal = [%compare.equal: t]

  let str = function
    | Normal i -> "x" ^ string_of_int i
    | Special i -> "s" ^ string_of_int i
end

module Var = struct

  type t =
    | VAR_Prog of ProgVar.t
    | VAR_Logic of LogVar.t
    [@@deriving compare, show]

  let str = function
    | VAR_Prog x -> ProgVar.str x
    | VAR_Logic x -> LogVar.str x

  let equal = [%compare.equal: t]

  let hash = Hashtbl.hash

end

module VarHashtbl = Hashtbl.Make(Var)

module Expr = struct 
  open Basic 

  type t =
    | E_Null
    | E_Var of Var.t
    | E_Typename of typename
    [@@deriving compare, show]

  let equal = [%compare.equal: t]

  let str = function
    | E_Null ->
      "null"
    | E_Var (v) -> 
      Var.str v
      (*String.concat "." (Basic.str_var v::flds)*)
    | E_Typename t ->
      Basic.str_typename t

  let subst_var (var, e) = function
    | E_Var var' when Var.equal var var' ->
      e
    | olde ->
      olde

  let subst_htable ht = function
    | E_Var var when VarHashtbl.mem ht var ->
      VarHashtbl.find ht var
    | olde ->
      olde

  let fv = function
    | E_Var v ->
      Choice.return v
    | _ ->
      Choice.fail

end


module Pure = struct

  type t =
    | P_Eq of Expr.t * Expr.t
    | P_Ne of Expr.t * Expr.t
    [@@deriving compare, show]

  let equal = [%compare.equal: t]

  let str = function
    | P_Eq (e1, e2) -> 
      Printf.sprintf "%s == %s" (Expr.str e1) (Expr.str e2)

    | P_Ne (e1, e2) -> 
      Printf.sprintf "%s != %s" (Expr.str e1) (Expr.str e2)

  let subst_var sb = function
    | P_Eq (e1, e2) ->
      let e1 = Expr.subst_var sb e1 in
      let e2 = Expr.subst_var sb e2 in
      P_Eq (e1, e2)

    | P_Ne (e1, e2) ->
      let e1 = Expr.subst_var sb e1 in
      let e2 = Expr.subst_var sb e2 in
      P_Ne (e1, e2)

  let subst_htable sb = function
    | P_Eq (e1, e2) ->
      let e1 = Expr.subst_htable sb e1 in
      let e2 = Expr.subst_htable sb e2 in
      P_Eq (e1, e2)

    | P_Ne (e1, e2) ->
      let e1 = Expr.subst_htable sb e1 in
      let e2 = Expr.subst_htable sb e2 in
      P_Ne (e1, e2)


  let fv = function
    | P_Eq (e1, e2) ->
      Choice.mplus (Expr.fv e1) (Expr.fv e2)

    | P_Ne (e1, e2) ->
      Choice.mplus (Expr.fv e1) (Expr.fv e2)
end

module Spatial = struct

  type t =
    | S_Lseg of Expr.t * Expr.t
    | S_Pto1 of Expr.t * Basic.field * Expr.t
    | S_Cell of Expr.t 
    | S_True
    [@@deriving compare, show]

  let equal = [%compare.equal: t]

  let str = function
    | S_Lseg (e1, e2) -> 
      Printf.sprintf "lsegne(%s, %s)" (Expr.str e1) (Expr.str e2)

    | S_Pto1 (e1, fld, e2) -> 
      Printf.sprintf "%s.%s |-1> %s" (Expr.str e1) (Basic.str_field fld) (Expr.str e2)

    | S_Cell (e1) -> 
      Printf.sprintf "Cell(%s)" (Expr.str e1) 

    | S_True ->
      "True"

  let subst_var sb = function
    | S_Lseg (e1, e2) ->
      let e1 = Expr.subst_var sb e1 in
      let e2 = Expr.subst_var sb e2 in
      S_Lseg (e1, e2)

    | S_Pto1 (e1, fld, e2) ->
      let e1 = Expr.subst_var sb e1 in
      let e2 = Expr.subst_var sb e2 in
      S_Pto1 (e1, fld, e2)

    | S_Cell (e1) ->
      let e1 = Expr.subst_var sb e1 in
      S_Cell (e1)

    | S_True as e ->
      e

  let subst_htable sb = function
    | S_Lseg (e1, e2) ->
      let e1 = Expr.subst_htable sb e1 in
      let e2 = Expr.subst_htable sb e2 in
      S_Lseg (e1, e2)

    | S_Pto1 (e1, fld, e2) ->
      let e1 = Expr.subst_htable sb e1 in
      let e2 = Expr.subst_htable sb e2 in
      S_Pto1 (e1, fld, e2)

    | S_Cell (e1) ->
      let e1 = Expr.subst_htable sb e1 in
      S_Cell (e1)

    | S_True as e ->
      e

  let allocated_cell = function
    | S_Lseg (e, _) 
    | S_Cell (e) ->
      Some e

    | S_True
    | S_Pto1 _ -> 
      None


  let fv = function
    | S_Lseg (e1, e2) ->
      Choice.mplus (Expr.fv e1) (Expr.fv e2)
    | S_Pto1 (e1, _, e2) ->
      Choice.mplus (Expr.fv e1) (Expr.fv e2)
    | S_Cell (e1) ->
      Expr.fv e1
    | S_True ->
      Choice.fail

end

module type STermindex = sig
  type t
    [@@deriving compare, show]

  type el

  val empty: t

  val lookup: t -> el -> el Choice.t

  val insert: t -> el -> t

  val remove: t -> el -> t

  val to_seq: t -> el Seq.t

  val equal: t -> t -> bool

  val str: t -> string

  val is_empty: t -> bool

  val subst_var: t -> (Var.t * Expr.t) -> t

  val subst_htable: t -> Expr.t VarHashtbl.t -> t

  val fv: t -> Var.t Choice.t
end

module MakeListTermindex (M:sig
    type t
      [@@deriving compare]

    val pp: Format.formatter -> t -> unit

    val sep: string

    val str: t -> string

    val subst_var: (Var.t * Expr.t) -> t -> t

    val subst_htable: Expr.t VarHashtbl.t -> t -> t

    val equal: t -> t -> bool 

    val fv: t -> Var.t Choice.t
  end) = struct

  type el = M.t
    [@@deriving compare, show]

  type t = el list
    [@@deriving compare, show]

  let empty = []

  let insert t el = Lib.Utils.insert_sorted M.compare el t

  let insert_uniq t el = Lib.Utils.insert_sorted_uniq M.compare el t

  let remove_all t el =
    List.filter (fun x -> M.compare el x <> 0) t

  let remove_one t el =
    let rec loop = function
      | [] -> []
      | x::xs when M.compare el x == 0 ->
        xs
      | x::xs -> x::loop xs
    in
    loop t

  let to_choice t = Lib.Extseq.to_choice (List.to_seq t)

  let equal = [%compare.equal: t]

  let str t = String.concat
    (Printf.sprintf " %s " M.sep)
    (List.map M.str t)

  let is_empty t = t = []

  let subst_var t sb = List.sort M.compare @@ List.map (M.subst_var sb) t

  let subst_htable t sb = List.sort M.compare @@ List.map (M.subst_htable sb) t

  let find t p =
    List.find_opt (M.equal p) t

  let fv t =
    List.fold_left Choice.mplus Choice.fail (List.map M.fv t)

  let join a b = List.concat [a;b]

end

module PureTermIndexM = struct
    include Pure
    let sep = "/\\" 
end


module SpatialTermIndexM = struct
  include Spatial 

  let sep = "**"
end

module PureTermIndex = MakeListTermindex(PureTermIndexM)
module SpatialTermIndex = MakeListTermindex(SpatialTermIndexM)

module RawSymbolicHeap = struct

  type t =
    { pure: PureTermIndex.t 
    ; spatial: SpatialTermIndex.t
    }
    [@@deriving compare, show]

  let empty =
    { pure = PureTermIndex.empty
    ; spatial = SpatialTermIndex.empty
    }

  let alter ?pure ?spatial t =
    let t = match pure with
      | Some pure -> {t with pure}
      | None -> t
    in
    let t = match spatial with
      | Some spatial -> {t with spatial}
      | None -> t
    in
    t

  let any_pure t = PureTermIndex.to_choice t.pure

  let any_spatial t = SpatialTermIndex.to_choice t.spatial

  let remove_pure t p = alter 
    ~pure:(PureTermIndex.remove_all t.pure p) t

  let remove_spatial t p = alter 
    ~spatial:(SpatialTermIndex.remove_one t.spatial p) t

  let equal = [%compare.equal: t]

  let pure t = t.pure

  let spatial t = t.spatial

  let insert_spatial t p =
    alter ~spatial:(SpatialTermIndex.insert t.spatial p) t

  let insert_pure t p =
    alter ~pure:(PureTermIndex.insert_uniq t.pure p) t

  let str t = 
    let spatial = SpatialTermIndex.str t.spatial in
    let pure = PureTermIndex.str t.pure in
    match String.length spatial, String.length pure with
    | 0, 0 -> "Empty"
    | 0, _ -> pure
    | _, 0 -> spatial
    | _ -> Printf.sprintf "%s /\\ %s" spatial pure

  let is_empty_pure t = PureTermIndex.is_empty t.pure

  let is_empty_spatial t = SpatialTermIndex.is_empty t.spatial

  let is_empty t = is_empty_pure t && is_empty_spatial t

  let subst_var t sb = 
    alter t
      ~pure:(PureTermIndex.subst_var t.pure sb)
      ~spatial:(SpatialTermIndex.subst_var t.spatial sb)

  let subst_htable t sb = 
    alter t
      ~pure:(PureTermIndex.subst_htable t.pure sb)
      ~spatial:(SpatialTermIndex.subst_htable t.spatial sb)

  let has_pure t p =
    Lib.Utils.is_some @@ PureTermIndex.find t.pure p

  let has_sptial t p =
    Lib.Utils.is_some @@ SpatialTermIndex.find t.spatial p

  let fv t =
    Choice.mplus (PureTermIndex.fv t.pure) (SpatialTermIndex.fv t.spatial)

  let only_pure t =
    alter ~pure:t.pure
      empty

  let only_spatial t =
    alter ~spatial:t.spatial
      empty

  let join a b =
    alter
      ~pure:(PureTermIndex.join a.pure b.pure)
      ~spatial:(SpatialTermIndex.join a.spatial b.spatial)
      empty
end

module SymbolicHeap = RawSymbolicHeap

module SymbolicState = struct

  type t =
    { local: SymbolicHeap.t }

  let local t = t.local 

  let alter ?local t = 
    let t = match local with
      | Some local -> {local}
      | None -> t
    in
    t

end