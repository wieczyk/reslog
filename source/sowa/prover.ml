open Logic

let debugf fmt = Lib.Log.make_debug_printf __MODULE__ fmt

module ProofSearch = struct

  type goal = SymbolicHeap.t * SymbolicHeap.t

  type 'm rule_matcher = goal -> 'm Choice.t

  type prover_step =
    | Fail
    | Leftover of SymbolicHeap.t
    | Goals of goal list

  type 'm rule_executor = goal -> 'm -> prover_step

  type 'm rule_impl = 
    { rmatch : 'm rule_matcher
    ; rexec : 'm rule_executor
    }

  type rule_descr = 
    | Rule : { name: string ; impl: 'm rule_impl } -> rule_descr


  let mk_rule name rmatch rexec = Rule {name; impl={rmatch; rexec}}

  let prove rules start_goal =
    let counter = ref 0 in 

    let rec add_to_queue tags queue = function
      | [] ->
        tags, queue
      | g :: gs ->
        incr counter;
        let tag = !counter in 
        add_to_queue (tag::tags) ( (g, tag) :: queue) gs
    in
    let rec loop los = function
      | [] ->
        debugf "!! No more goals";
        Some los
      | (goal, tag)::queue ->
        debugf "Goal %u" tag;
        debugf "     - LHS: %s" (SymbolicHeap.str (fst goal));
        debugf "     - RHS: %s" (SymbolicHeap.str (snd goal));
        apply goal tag queue los rules

    and apply goal tag queue los = function
      | [] ->
        None
      | (Rule {name; impl={rmatch; rexec}})::rs ->
        debugf "- trying %s" name;
        let module Let_syntax = Lib.Extseq.Let_syntax in 
        let cc = Choice.(
          let%bind m = rmatch goal in 
          return (rexec goal m)
        ) in
        match Choice.run_one cc with
        | Some Fail
        | None ->
          apply goal tag queue los rs
        | Some (Goals newgoals) ->
          let tags, queue = add_to_queue [] queue newgoals in
          let tags = String.concat "," (List.map string_of_int tags) in
          debugf "New goals %u -> %s" tag tags;
          loop los queue
        | Some (Leftover sh) ->
          debugf "Leftover %u -> %s" tag (SymbolicHeap.str sh);
          loop (sh::los) queue


    in
    loop [] [ (start_goal, 0) ]
end


module Rules = struct
  open ProofSearch
  open Choice

  module Let_syntax = Lib.Extseq.Let_syntax

  module CommonHelpers = struct

      let unpack_eq = function
        | Pure.P_Eq (e1, e2) -> return (e1, e2)
        | _ -> fail

      let unpack_ne = function
        | Pure.P_Ne (e1, e2) -> return (e1, e2)
        | _ -> fail

      let unpack_lsegne = function
        | Spatial.S_Lseg (e1, e2) -> return (e1, e2)
        | _ -> fail

      let reorder_var_expr (e1, e2) = match e1, e2 with
        | Expr.E_Var var, e 
        | e, Expr.E_Var var ->
          return (var, e)

        | _ -> 
          fail

      let unpack_pto1 = function
        | Spatial.S_Pto1 (e1, fld, e2) ->
          return (e1, fld, e2)
        | _ ->
          fail

      let unpack_allocated m =
        match Spatial.allocated_cell m with
        | Some m -> return m
        | None -> fail

      let product_without_diag t = 
        let p = product t t in
        let%bind (a,b) = p in
        let%bind _ = guard (a != b) in
        return (a,b)

  end

  open CommonHelpers

  module Normalization = struct

    module SubstEq = struct
      (*
        RHS[lvar := E] |- RHS[lvar := E]
        ----------------------
        RHS /\ lvar = E |- RHS
       *)

      let matcher (lhs, _) =
        let%bind eq = SymbolicHeap.any_pure lhs in
        let%bind eq_args = unpack_eq eq in 
        let%bind sb = reorder_var_expr eq_args in 
        return (eq, sb)

      let exec (lhs, rhs) (eq, sb) =
        let lhs = SymbolicHeap.remove_pure lhs eq in
        let lhs = SymbolicHeap.subst_var lhs sb in 
        let rhs = SymbolicHeap.subst_var rhs sb in
        Goals [lhs, rhs]

      let rule = mk_rule "N_SubstEq" matcher exec

    end

    module FailPto = struct
    (*
      ------------------------------------
      LHS ** e.f |-> a ** e.f |-> b |- RHS
     *)

     let matcher (lhs, _) = 
        let%bind (s1, s2) = product_without_diag (SymbolicHeap.any_spatial lhs) in
        let%bind (a_e, a_fld, _) = unpack_pto1 s1 in 
        let%bind (b_e, b_fld, _) = unpack_pto1 s2 in
        let%bind _ = guard (Expr.equal a_e b_e) in
        let%bind _ = guard (Basic.equal_field a_fld b_fld) in
        return ()

      let exec _ _ =
        Goals []

      let rule = mk_rule "N_FailPto" matcher exec
    end


    module AxiomFalse = struct
      (*
        ----------------------
        null != null |- RHS
       *)

      let matcher (lhs, _) =
        let%bind ne = SymbolicHeap.any_pure lhs in
        let%bind (e1, e2) = unpack_ne ne in 
        let%bind _ = guard (Expr.(equal e1 e2)) in
        return ()

      let exec _ () =
        Goals []

      let rule = mk_rule "N_AxiomFalse" matcher exec

    end


    module LsegneNotNil = struct
      (*
        LHS ** lsegne(e1, e2) /\ e1 != e2 /\ e1 != null |- RHS
        ------------------------------------------------------
        LHS ** lsegne(e1, e2) |- RHS
      *)

      let matcher (lhs, _) =
        let%bind s = SymbolicHeap.any_spatial lhs in
        let%bind (e1, e2) = unpack_lsegne s in 
        let p1 = Pure.P_Ne (e1, e2) in 
        let p2 = Pure.P_Ne (e1, Expr.E_Null) in 
        let%bind _ = guard (not @@ (SymbolicHeap.has_pure lhs p1 && SymbolicHeap.has_pure lhs p2)) in
        debugf "test?: %b" (SymbolicHeap.has_pure lhs p2);
        return (p1, p2)

      let exec (lhs, rhs) (p1, p2) =
        let lhs = SymbolicHeap.insert_pure lhs p1 in
        let lhs = SymbolicHeap.insert_pure lhs p2 in
        Goals [lhs, rhs]

      let rule = mk_rule "N_LsegneNotNil" matcher exec
    end

    module DifferentCells = struct
      (*
        LHS ** S(e) ** S'(e') /\ e != e' |- RHS
        ---------------------------------------
        LHS ** S(e) ** S'(e') |- RHS
      *)

      let matcher (lhs, _) =
        let%bind s1 = SymbolicHeap.any_spatial lhs in
        let%bind s2 = SymbolicHeap.any_spatial lhs in
        (* intentional physical equality *)
        let%bind _ = guard (s1 != s2) in
        match (Spatial.allocated_cell s1, Spatial.allocated_cell s2) with
        | Some e1, Some e2 ->
          let p = (Pure.P_Ne (e1, e2)) in
          let%bind _ = guard (not @@ SymbolicHeap.has_pure lhs p) in
          return p
        | _ ->
          fail

      let exec (lhs, rhs) p =
        let lhs = SymbolicHeap.insert_pure lhs p in 
        Goals [lhs, rhs]

      let rule = mk_rule "N_DifferentCells" matcher exec
    end

    module ExcludedMiddle = struct
      (*
        LHS /\ E1 == E2 |- RHS      LHS /\ E1 != E2 |- RHS
        ---------------------------------------------------
        LHS |- RHS

        for any E1,E2 in FV(LHS, RHS) such that we do not have eq or ne for them
       *)

      let matcher (lhs, rhs) =
        let fv = mplus (SymbolicHeap.fv lhs) (SymbolicHeap.fv rhs) in
        let%bind v1 = fv in
        let%bind v2 = fv in
        let%bind _ = guard (not @@ Var.equal v1 v2) in
        let e1 = Expr.E_Var v1 in 
        let e2 = Expr.E_Var v2 in 
        let eq = Pure.P_Eq (e1, e2) in
        let ne = Pure.P_Ne (e1, e2) in
        let%bind _ = guard (not @@ SymbolicHeap.has_pure lhs eq) in
        let%bind _ = guard (not @@ SymbolicHeap.has_pure lhs ne) in
        return (eq, ne)

      let exec (lhs, rhs) (eq, ne) =
        let lhs_eq = SymbolicHeap.insert_pure lhs eq in 
        let lhs_ne = SymbolicHeap.insert_pure lhs ne in 
        Goals [lhs_eq, rhs; lhs_ne, rhs]


      let rule = mk_rule "N_ExcluddedMiddle" matcher exec
    end

    module CorrespondPto = struct
    (*
      !!DISABLED!! 
      LHS ** e.f = a /\ a = b |- RHS
      --------------------------------
      LHS ** e.f = a ** e.f = b |- RHS
     *)

     let matcher (lhs, _) = 
        let%bind (s1, s2) = product_without_diag (SymbolicHeap.any_spatial lhs) in
        let%bind (a_e, a_fld, a_v) = unpack_pto1 s1 in 
        let%bind (b_e, b_fld, b_v) = unpack_pto1 s2 in
        let%bind _ = guard (Expr.equal a_e b_e) in
        let%bind _ = guard (Basic.equal_field a_fld b_fld) in
        let eq = Pure.P_Eq (a_v, b_v) in
        let%bind _ = guard (not @@ SymbolicHeap.has_pure lhs eq) in
        return (eq, s2)

      let exec (lhs, rhs) (eq, s2) =
        let lhs = SymbolicHeap.remove_spatial lhs s2 in
        let lhs = SymbolicHeap.insert_pure lhs eq in
        Goals [lhs, rhs]

      let rule = mk_rule "N_CorrespondPto" matcher exec
    end

  end (* Normalization *)


  module Substraction = struct

    module AxiomEmpty = struct
      (*
        -----------------------
        LPure /\ Empty |- Empty
      *)
    
      let matcher allow_leftover (lhs, rhs) =
        let%bind _ = guard (SymbolicHeap.is_empty_spatial rhs) in 
        if SymbolicHeap.is_empty_spatial lhs then
          return (Goals [])
        else
          let%bind _ = guard allow_leftover in
          return (Leftover lhs)

      let exec _ r =
        r

      let rule allow_leftover = mk_rule "S_AxiomEmpty" (matcher allow_leftover) exec

    end

    module DummyEq = struct
      (*
        LHS |- RHS
        -----------------------
        LHS |- RHS /\ E = E
      *)

      let matcher (_, rhs) =
        let%bind m = SymbolicHeap.any_pure rhs in 
        match m with
        | Pure.P_Eq (e1, e2) when Expr.equal e1 e2 ->
          return m
        | _ ->
          fail

      let exec (lhs, rhs) eq =
          let rhs = SymbolicHeap.remove_pure rhs eq in
          Goals [(lhs, rhs)]

      let rule = mk_rule "S_DummyEq" matcher exec

    end

    module StarIntro = struct
      (*
        Eliminates the same spatial predicate from both sides.
        This rule should be executed when all other, except True-rules, failed. 

        LHS |- RHS
        ----------------------
        LHS ** S |- RHS ** S
      *)

      let matcher (lhs, rhs) =
        let%bind l = SymbolicHeap.any_spatial rhs in
        let%bind r = SymbolicHeap.any_spatial lhs in 
        let%bind _ = guard (Spatial.equal l r) in
        return l

      let exec (lhs, rhs) s =
        let lhs = SymbolicHeap.remove_spatial lhs s in
        let rhs = SymbolicHeap.remove_spatial rhs s in
        Goals [lhs, rhs]

      let rule = mk_rule "S_StarIntro" matcher exec

    end

    module TrueIntro = struct
      (*
        LHS |- RHS
        ----------------------
        LHS ** S |- RHS ** True
      *)

      let matcher (lhs, rhs) =
        let%bind r = SymbolicHeap.any_spatial rhs in 
        match r with
        | Spatial.S_True ->
          let%bind l = SymbolicHeap.any_spatial lhs in
          return l
        | _ ->
          fail

      let exec (lhs, rhs) s =
        let lhs = SymbolicHeap.remove_spatial lhs s in
        Goals [lhs, rhs]

      let rule = mk_rule "S_TrueIntro" matcher exec

    end

    module TrueEmpty = struct
      (*
        Eliminates true in RHS when there is no spatial stuff on LHS

        LHS | Empty |- RHS
        ----------------------
        LHS | Empty |- RHS ** True
      *)

      let matcher (lhs, rhs) =
        let%bind r = SymbolicHeap.any_spatial rhs in 
        match r with
        | Spatial.S_True ->
          if SymbolicHeap.is_empty_spatial lhs then
            return r
          else
            fail
        | _ ->
          fail

      let exec (lhs, rhs) s =
        let rhs = SymbolicHeap.remove_spatial rhs s in
        Goals [lhs, rhs]

      let rule = mk_rule "S_TrueEmpty" matcher exec

    end

    module AxiomPred = struct

      (*
        LHS /\ P |- RHS
        ---------------------
        LHS /\ P |- RHS /\ P
      *)

      let matcher (lhs, rhs) =
        let%bind l = SymbolicHeap.any_pure rhs in
        let%bind r = SymbolicHeap.any_pure lhs in 
        let%bind _ = guard (Pure.equal l r) in
        return l

      let exec (lhs, rhs) p =
        let rhs = SymbolicHeap.remove_pure rhs p in
        Goals [lhs, rhs]

      let rule = mk_rule "S_AxiomPred" matcher exec
    end

    module RollLsegne = struct
      (*
        LHS ** LHS' |- RHS ** LHS' ** lsegne(en, ee)
        --------------------------------------------
        LHS ** LHS' |- RHS ** lsegne(eb, ee)

        LHS' = eb.next |-> en ** eb.value |-> ev
       *)

      let matcher (lhs, rhs) =
        let%bind lseg = SymbolicHeap.any_spatial rhs in
        let%bind (eb, ee) = unpack_lsegne lseg in

        let%bind (s1, s2) = product_without_diag (SymbolicHeap.any_spatial lhs) in 

        let%bind (a_e, a_fld, en) = unpack_pto1 s1 in 
        let%bind _ = guard (Expr.equal a_e eb) in 
        let%bind _ = guard (a_fld = Predefined.field_next) in 

        let%bind (b_e, b_fld, _) = unpack_pto1 s2 in 
        let%bind _ = guard (Expr.equal b_e eb) in 
        let%bind _ = guard (b_fld = Predefined.field_value) in 

        let leftover = Spatial.S_Lseg (en, ee) in

        let %bind _ = guard (not (SymbolicHeap.has_sptial rhs s1 && SymbolicHeap.has_sptial rhs s2)) in 

        return (s1, s2, leftover, lseg)


      let exec (lhs, rhs) (s1, s2, leftover, lseg) =
        let rhs = SymbolicHeap.remove_spatial rhs lseg in
        let rhs = SymbolicHeap.insert_spatial rhs leftover in
        let rhs = SymbolicHeap.insert_spatial rhs s1 in 
        let rhs = SymbolicHeap.insert_spatial rhs s2 in 
        Goals [lhs, rhs]

      let rule = mk_rule "S_RollLsegne" matcher exec

    end

    module CorrespondPto = struct
    (*
      !!DISABLED!!

      LHS |- RHS ** e.f = a /\ a = b 
      ------------------------------------
      LHS |- RHS ** e.f |-> a ** e.f |-> b 
     *)

     let matcher (_, rhs) = 
        let%bind (s1, s2) = product_without_diag (SymbolicHeap.any_spatial rhs) in
        let%bind (a_e, a_fld, a_v) = unpack_pto1 s1 in 
        let%bind (b_e, b_fld, b_v) = unpack_pto1 s2 in
        let%bind _ = guard (Expr.equal a_e b_e) in
        let%bind _ = guard (Basic.equal_field a_fld b_fld) in
        let eq = Pure.P_Eq (a_v, b_v) in
        let%bind _ = guard (not @@ SymbolicHeap.has_pure rhs eq) in
        return (eq, s2)

      let exec (lhs, rhs) (eq, s2) =
        let rhs = SymbolicHeap.remove_spatial rhs s2 in
        let rhs = SymbolicHeap.insert_pure rhs eq in
        Goals [lhs, rhs]

      let rule = mk_rule "S_CorrespondPto" matcher exec
    end


    module AppendLsegne = struct
    (*
      LHS ** LHS' |- RHS ** lsegne(e1, e2) ** lsegne(e2, e3)
      -------------------------------------------------------------
      LHS ** LHS' |- RHS ** lsegne(e1, e3)

      LHS' = lsegne(e1, e2) ** S(e3)
     *)

     let matcher (lhs, rhs) = 
        let%bind ls_e1_e3 = SymbolicHeap.any_spatial rhs in
        let%bind (e1, e3) = unpack_lsegne ls_e1_e3 in
        debugf "Considering1: %s" (Spatial.str ls_e1_e3);
        let%bind (ls_e1_e2, s_e3) = product_without_diag (SymbolicHeap.any_spatial lhs) in

        debugf "Considering2: %s and %s" (Spatial.str ls_e1_e2) (Spatial.str s_e3);
        let%bind (e1', e2) = unpack_lsegne ls_e1_e2 in
        let%bind _ = guard (Expr.equal e1 e1') in

        let%bind e3' = unpack_allocated s_e3 in
        let%bind _ = guard (Expr.equal e3 e3') in

        let ls_e2_e3 = Spatial.S_Lseg (e2, e3) in
        let%bind _ = guard (not @@ SymbolicHeap.has_sptial rhs ls_e2_e3) in

        return (ls_e1_e2, ls_e2_e3, ls_e1_e3)

      let exec (lhs, rhs) (ls1, ls2, ls12) =
        let rhs = SymbolicHeap.remove_spatial rhs ls12 in
        let rhs = SymbolicHeap.insert_spatial rhs ls1 in
        let rhs = SymbolicHeap.insert_spatial rhs ls2 in
        Goals [lhs, rhs]

      let rule = mk_rule "S_AppendLsegne" matcher exec
    end

  end (* Substraction *)
  
  let rules allow_leftovers = 
    [ Normalization.SubstEq.rule
    ; Normalization.AxiomFalse.rule
    ; Normalization.LsegneNotNil.rule
    ; Normalization.DifferentCells.rule
    ; Normalization.FailPto.rule
    ; Substraction.AxiomPred.rule
    ; Substraction.DummyEq.rule
    ; Substraction.RollLsegne.rule
    (* ; Substraction.CorrespondPto.rule *)
    (*; Substraction.AppendLsegne.rule*)
    ; Substraction.AxiomEmpty.rule allow_leftovers
    ; Normalization.ExcludedMiddle.rule
    ; Substraction.StarIntro.rule
    ; Substraction.TrueIntro.rule
    ; Substraction.TrueEmpty.rule
    ]
end

let prove goal =
  debugf "Calling prover";
  match ProofSearch.prove (Rules.rules false) goal with
  | Some [] -> 
    debugf "Prover: success";
    true
  | _ -> 
    debugf "Prover: failed";
    false

let simplify_frames xs =
  let xs = List.map (SymbolicHeap.only_spatial) xs in
  List.sort_uniq SymbolicHeap.compare xs

let infer_frame goal =
  debugf "Calling frame inference";
  match ProofSearch.prove (Rules.rules true) goal with
  | Some [] -> 
    debugf "Frame inference: no frame needed";
    Some SymbolicHeap.empty

  | Some xs -> 
    debugf "Frame inference: found leftover; removing pure and sorting";
    let xs = simplify_frames xs in 
    begin match xs with
    | [x] ->
      debugf "Frame infere: one frame!";
      Some x
    | xs ->
      debugf "Frame infere: found more than one frame!";
      let p x = 
        debugf "- %s" (SymbolicHeap.str x)
      in
      List.iter p xs;
      None
    end
  | _ -> 
    debugf "Frame inference: no leftovers; failure";
    None

let consistent sh =
  let ne = Pure.P_Ne (Expr.E_Null, Expr.E_Null) in 
  let r = not @@ prove (sh, SymbolicHeap.insert_pure SymbolicHeap.empty ne) in
  if r then
    debugf "Heap is consistent"
  else
    debugf "Heap is inconsistent";
  r

let inconsistent sh =
  not @@ consistent sh