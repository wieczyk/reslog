module L = Logic
module P = Slang.Parser
module A = Slang.Ast

module Translator() = struct

  let context = Hashtbl.create 513


  let as_var = function
    | A.MkIdentifier s ->
      begin try
        Hashtbl.find context s
      with Not_found ->
        let i = Hashtbl.length context in
        let v = L.Var.VAR_Logic (L.LogVar.Normal i) in
        Hashtbl.replace context s v;
        v
      end
    | A.MkProgramIdentifier s ->
      L.Var.VAR_Prog (L.ProgVar.Progvar s)

  let as_expression = function
    | A.EXPR_Identifier i ->
      L.Expr.E_Var (as_var i)
    | A.EXPR_Nil ->
      L.Expr.E_Null
    | _ ->
      failwith "Not supported EXPR_"

  let as_predicate_lseg = function
    | [e1; e2] ->
      L.Spatial.S_Lseg (e1, e2)
    | _ ->
      failwith "lseg requires 2 arguments"

  let as_predicate_cell = function
    | [e1] ->
      L.Spatial.S_Cell (e1)
    | _ ->
      failwith "cell requires 1 arguments"

  let as_predicate_true = function
    | [] ->
      L.Spatial.S_True
    | _ ->
      failwith "True takes not arguments"

  let as_predicate = function
    | A.MkIdentifier "lsegne" ->
      as_predicate_lseg
    | A.MkIdentifier "Cell" ->
      as_predicate_cell
    | A.MkIdentifier "True" ->
      as_predicate_true
    | _ ->
      failwith "Unknown predicate"

  let as_relation = function
    | A.REL_BINOP_Eq ->
      fun (e1, e2) -> L.Pure.P_Eq (e1, e2)
    | A.REL_BINOP_Ne ->
      fun (e1, e2) -> L.Pure.P_Ne (e1, e2)
    | _ ->
      failwith "Unsupported REL"

  let as_fieldname = function
    | A.MkIdentifier i -> 
      L.Basic.Field i
    | _ ->
      failwith "Invalid field name"

  let as_field = function
    | A.EXPR_Field (e, fld) ->
      let e = as_expression e in
      let fld = as_fieldname fld in
      (e, fld)
    | _ ->
      failwith "Unsupported form of a field"

  let rec as_formula sh = function
    | A.FRML_Predicate (f, xs) ->
      let xs = List.map as_expression xs in
      let p = as_predicate f xs in
      L.SymbolicHeap.insert_spatial sh p
    | A.FRML_Relation (r, e1, e2) ->
      let e1 = as_expression e1 in
      let e2 = as_expression e2 in 
      let p = as_relation r (e1, e2) in
      L.SymbolicHeap.insert_pure sh p
    | A.FRML_Binop (_, f1, f2) ->
      let sh = as_formula sh f1 in
      let sh = as_formula sh f2 in
      sh
    | A.FRML_Ptsto (e1, e2) ->
      let (e1, fld) = as_field e1 in
      let e2 = as_expression e2 in 
      let s = L.Spatial.S_Pto1(e1, fld, e2) in
      L.SymbolicHeap.insert_spatial sh s

    | A.FRML_Empty ->
      sh
    | A.FRML_True ->
      let s = L.Spatial.S_True in
      L.SymbolicHeap.insert_spatial sh s
    | _ ->
      failwith "Not supported FRML_"

end

let local_parse_string s =
  match P.parse_string s with
  | Ok a ->
    let module T = Translator() in
    Some (T.as_formula L.SymbolicHeap.empty a)
  | Error _ ->
    None

let global_parse_string =
  let module T = Translator() in fun s ->
  match P.parse_string s with
  | Ok a ->
    Some (T.as_formula L.SymbolicHeap.empty a)
  | Error _ ->
    None
