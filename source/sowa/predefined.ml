open Logic

let typename_ListNode = Expr.E_Typename (Basic.ProgtypeName "ListNode")

let field_next = Basic.Field "next"

let field_value = Basic.Field "value"