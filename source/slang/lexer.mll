{
  open Grammar
  open Lexing
  
  exception InvalidToken of Preast.location * string
  
  let keywords =
      [ "forall", FORALL
      ; "exists", EXISTS
      ]
  
  let symbols =
      [ "**", OP_LOG_STARSTAR
      ; "/", OP_DIV
      ; "*", OP_MUL
      ; "+", OP_ADD
      ; "-", OP_SUB
      ; "/\\", OP_LOG_AND
      ; "\\/", OP_LOG_OR
      ; "~", OP_LOG_NOT
      ; "(", LPARENT
      ; ")", RPARENT
      ; "{", CURLY_LPARENT
      ; "}", CURLY_RPARENT
      ; ",", COMA
      ; ".", DOT
      ; ":", COLON 
      ; "==", REL_EQ
      ; "!=", REL_NE
      ; "<=", REL_LE
      ; "<", REL_LT
      ; ">", REL_GT
      ; ">=", REL_GE
      ; "->", OP_LOG_IMPLY
      ; "-*", OP_LOG_IMPLYSTAR
      ; "|->", OP_PTSTO
      ]
  
  let makeTable mapping =
      let h = Hashtbl.create 127 in
      List.iter (fun (str, key) -> Hashtbl.replace h str key) mapping;
      h
  
  let keywordsTable = makeTable keywords
  let symbolTable = makeTable symbols
  
  let mkIdentifier str =
      try
          Hashtbl.find keywordsTable str
      with Not_found ->
          IDENTIFIER str
  
  let lookupSymbol str =
      try
          Hashtbl.find symbolTable str
      with Not_found ->
          failwith ("unknown token symbol: " ^ str)
  
  let mkLoc pos = 
      (pos.Lexing.pos_lnum, pos.Lexing.pos_cnum - pos.Lexing.pos_bol + 1)
  
  let handleError pos token =
      let exc = InvalidToken (mkLoc pos, token) in
      raise exc
  
  
  let update_loc lexbuf = 
      let pos = lexbuf.lex_curr_p  in
      lexbuf.lex_curr_p <- { pos with
        pos_lnum  = pos.pos_lnum + 1;
        pos_bol   = pos.pos_cnum
      }
  }
  
  let digit   = ['0'-'9']
  let id        = ['a'-'z' '_' 'A' - 'Z']['_' 'A' - 'Z' 'a'-'z' '0'-'9']*
  let qstring = [^ '"']* 
  let symbols = ['(' ')' '[' ']' '{' '}' ';' ':' ',' '+' '-' '*' '/' '<' '>' ]
                 | "==" | "!=" | "<=" | ">=" | "|->"
                 | "/\\" | "\\/" | "~" | "." | "**" | "->"
                 | "-*"
  
  rule token = parse
      | ['\n']
      { update_loc lexbuf; token lexbuf }

      | "//"
      { line_comment "" lexbuf }
  
      | [' ' '\t' '\r']
      { token lexbuf }
  
      | digit+ as num
      { INT (int_of_string num) }
  
      | ('-' digit+) as num
      { INT (int_of_string num) }
  
      | '%'(id as str)
      { DOLAR_IDENTIFIER str }

      | id as str
      { mkIdentifier str }

      | symbols as symbol
      { lookupSymbol symbol }
  
      | eof
      { EOF }
  
      | _
      { handleError (Lexing.lexeme_start_p lexbuf) (Lexing.lexeme lexbuf) }


    and line_comment buff = parse
      | '\n' 
      { update_loc lexbuf; token lexbuf }

      | _ 
      { line_comment (buff ^ Lexing.lexeme lexbuf) lexbuf }
