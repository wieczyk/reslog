type location = int * int
type span_location = location * location

type identifier =
  | MkIdentifier of string
  | MkDolarIdentifier of string

type binop = 
  | BINOP_Add 
  | BINOP_Sub
  | BINOP_Mul 
  | BINOP_Div
  | BINOP_Eq
  | BINOP_Ne
  | BINOP_Lt
  | BINOP_Gt
  | BINOP_Le
  | BINOP_Ge
  | BINOP_Star
  | BINOP_ImplyStar
  | BINOP_And
  | BINOP_Or
  | BINOP_Imply

type unop =
  | UNOP_Neg
  | UNOP_Not

type 'tag expression =
  | EXPR_Identifier of 'tag * identifier
  | EXPR_Int of 'tag * int
  | EXPR_Binop of 'tag * binop * 'tag expression * 'tag expression
  | EXPR_Unop of 'tag * unop * 'tag expression
  | EXPR_Call of 'tag * 'tag expression * 'tag expression list
  | EXPR_Dict of 'tag * (identifier * 'tag expression) list
  | EXPR_Exists of 'tag * identifier list * 'tag expression
  | EXPR_Forall of 'tag * identifier list * 'tag expression
  | EXPR_Ptsto of 'tag  * 'tag expression * 'tag expression
  | EXPR_Field of 'tag * 'tag expression * identifier
