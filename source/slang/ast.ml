type location = int * int
type span_location = location * location

type identifier =
   | MkIdentifier of string
   | MkProgramIdentifier of string

type expression_binop = 
  | EXPR_BINOP_Add
  | EXPR_BINOP_Sub 
  | EXPR_BINOP_Mul 
  | EXPR_BINOP_Div

type expression_unop =
  | EXPR_UNOP_Neg
  | EXPR_UNOP_Not

type logic_binop =
  | FRML_BINOP_Imply 
  | FRML_BINOP_ImplyStar
  | FRML_BINOP_Or 
  | FRML_BINOP_And 
  | FRML_BINOP_Star

type rel_binop = 
  | REL_BINOP_Eq  
  | REL_BINOP_Ne
  | REL_BINOP_Lt  
  | REL_BINOP_Gt
  | REL_BINOP_Le  
  | REL_BINOP_Ge

type logic_unop =
  | FRML_UNOP_Not

type expression =
  | EXPR_Identifier of identifier 
  | EXPR_Int of int
  | EXPR_Binop of expression_binop * expression * expression
  | EXPR_Unop of expression_unop * expression
  | EXPR_Call of expression * expression list
  | EXPR_Dict of (string * expression) list
  | EXPR_Field of expression * identifier
  | EXPR_Nil

type formula =
  | FRML_Predicate of identifier * expression list
  | FRML_Binop of logic_binop * formula * formula
  | FRML_Unop of logic_unop * formula 
  | FRML_Relation of rel_binop * expression * expression
  | FRML_Forall of identifier list * formula
  | FRML_Exists of identifier list * formula
  | FRML_True
  | FRML_Empty
  | FRML_False
  | FRML_Ptsto of expression * expression