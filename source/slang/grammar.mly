%{

open Preast

let mkLoc pos = 
    (pos.Lexing.pos_lnum, pos.Lexing.pos_cnum - pos.Lexing.pos_bol + 1)

%}

%type <Preast.span_location Preast.expression> parse_formula
%start parse_formula

%token EOF
%token <string> IDENTIFIER
%token <string> DOLAR_IDENTIFIER
%token <int> INT

/* expression */
%token OP_ADD OP_SUB OP_MUL OP_DIV
/* first order logic */
%token OP_LOG_AND OP_LOG_OR OP_LOG_NOT OP_LOG_IMPLY EXISTS FORALL
/* spatial */
%token OP_PTSTO OP_LOG_IMPLYSTAR OP_LOG_STARSTAR
/* relations */
%token REL_EQ REL_NE REL_GT REL_GE REL_LT REL_LE
/* lexical */
%token LPARENT RPARENT CURLY_LPARENT CURLY_RPARENT 
%token DOT COMA COLON

%%

parse_formula: formula EOF { $1 }

/**********************************************
 * Formulas and expressions
 *
 * We are parsing a more generic form of expression which later will be
 * transformed into logical formulas and term expressions
 *
 * Tiers:
 * - ->, -*
 * - \/
 * - /\
 * - **
 * - ~
 * - forall, exists
 * - <. <=, ==, !=, >=, >
 * - +, -
 * - *, /
 * - not, -
 * - atomic
 */

formula: span(_formula) { $1 }

_formula:
  | _implicational_fol_formula_or_quantifier { $1 }

/* 
 * First Order Logic
 */
%inline
_implicational_fol_formula_or_quantifier:
  | _binoptier_assoc_right_altlast
    ( _additive_fol_formula
    , binop_logic_imply
    , _additive_formula_or_quantifier
    )
  { $1 }

%inline
_additive_fol_formula:
  | _binoptier_assoc_right(_multiplicative_formula, binop_logic_additive)
  { $1 }

%inline
_additive_formula_or_quantifier:
  | _binoptier_assoc_right_altlast
    ( _multiplicative_formula
    , binop_logic_additive
    , _multiplicative_formula_with_quantifier
    )
  { $1 }


%inline
_multiplicative_formula_with_quantifier:
  | _binoptier_assoc_right_altlast
    ( _unop_formula
    , binop_logic_multiplicative
    , _unop_formula_or_quantifier
    )
  { $1 }

%inline
_multiplicative_formula:
  | _binoptier_assoc_right(_unop_formula, binop_logic_multiplicative)
  { $1 } 

_unop_formula_or_quantifier:
  | _unop_formula { $1 }
  | _quantifier_formula { $1 }


_quantifier_formula:
  | EXISTS many1(identifier) prefix(DOT, span(_formula))
  { fun loc -> EXPR_Exists(loc, $2, $3) }
  | FORALL many1(identifier) prefix(DOT, span(_formula))
  { fun loc -> EXPR_Forall(loc, $2, $3) }

_unop_formula:
  _unoptier(_atomic_formula, unop_logic)
  { $1 } 

%inline
_relation_formula:
  _binoptier_once(_expression, binop_logic_relation)
  { $1 }

_atomic_formula:
  | _relation_formula
  { $1 } 
  | _expression 
  { $1 } 
  | _builtin_spatial
  { $1 }

_builtin_spatial:
  | _builtin_ptsto
  { $1 }

_builtin_ptsto:
  | expression binop_ptsto expression
  { $2 $1 $3 }


/* 
 * Term expression
 */
expression: span(_expression) { $1 } 

_expression: _additive_expression { $1 } 

_additive_expression: _binoptier_assoc_left(_multiplicative_expression, binop_additive) { $1 }

_multiplicative_expression: _binoptier_assoc_left(_unop_expression, binop_multiplicative) { $1 }

_unop_expression: _unoptier(_call_expression, unop) { $1 }

_call_expression:
  | _atom_expression  { $1 }
  | span(_atom_expression) brackets(LPARENT, RPARENT, manySep(COMA, expression)) 
  { fun loc -> EXPR_Call (loc, $1, $2) }

_atom_expression:
  | identifier  { fun loc -> EXPR_Identifier (loc, $1) }
  | field { $1 }
  | int         { fun loc -> EXPR_Int (loc, $1) }
  | brackets(CURLY_LPARENT, CURLY_RPARENT, manySep(COMA,pairSep(COLON, identifier, expression)))
  { fun loc -> EXPR_Dict (loc, $1) }

  | LPARENT _expression RPARENT { $2 }

field:
  | span(field_) DOT identifier { fun loc -> EXPR_Field(loc, $1, $3) }

field_:
  | identifier { fun loc -> EXPR_Identifier(loc, $1) }
  | span(field_) DOT identifier { fun loc -> EXPR_Field(loc, $1, $3) }

/**********************************************
 * Operators
 */

%inline
binop_logic_imply:
  | OP_LOG_IMPLY  { fun lhs rhs loc -> EXPR_Binop(loc, BINOP_Imply, lhs, rhs) }
  | OP_LOG_IMPLYSTAR  { fun lhs rhs loc -> EXPR_Binop(loc, BINOP_ImplyStar, lhs, rhs) }

%inline
binop_logic_additive:
  | OP_LOG_OR  { fun lhs rhs loc -> EXPR_Binop(loc, BINOP_Or, lhs, rhs) }

%inline
binop_logic_multiplicative:
  | OP_LOG_AND { fun lhs rhs loc -> EXPR_Binop(loc, BINOP_And, lhs, rhs) }
  | OP_LOG_STARSTAR { fun lhs rhs loc -> EXPR_Binop(loc, BINOP_Star, lhs, rhs) }

%inline
unop_logic:
  | OP_LOG_NOT { fun sub loc -> EXPR_Unop(loc, UNOP_Not, sub) }

%inline
binop_logic_relation:
  | REL_EQ { fun lhs rhs loc -> EXPR_Binop(loc, BINOP_Eq, lhs, rhs) }
  | REL_NE { fun lhs rhs loc -> EXPR_Binop(loc, BINOP_Ne, lhs, rhs) }
  | REL_GT { fun lhs rhs loc -> EXPR_Binop(loc, BINOP_Gt, lhs, rhs) }
  | REL_GE { fun lhs rhs loc -> EXPR_Binop(loc, BINOP_Ge, lhs, rhs) }
  | REL_LT { fun lhs rhs loc -> EXPR_Binop(loc, BINOP_Lt, lhs, rhs) }
  | REL_LE { fun lhs rhs loc -> EXPR_Binop(loc, BINOP_Le, lhs, rhs) }

unop:
  | OP_SUB { fun sub loc -> EXPR_Unop(loc, UNOP_Neg, sub) }

binop_ptsto:
  | OP_PTSTO { fun lhs rhs loc -> EXPR_Ptsto(loc, lhs, rhs) }

binop_additive:
  | OP_ADD { fun lhs rhs loc -> EXPR_Binop(loc, BINOP_Add, lhs, rhs) }
  | OP_SUB { fun lhs rhs loc -> EXPR_Binop(loc, BINOP_Sub, lhs, rhs) }

binop_multiplicative:
  | OP_MUL { fun lhs rhs loc -> EXPR_Binop(loc, BINOP_Mul, lhs, rhs) }
  | OP_DIV { fun lhs rhs loc -> EXPR_Binop(loc, BINOP_Div, lhs, rhs) }

/**********************************************
 * Lexical stuff
 */

identifier:
  | IDENTIFIER { MkIdentifier $1 }
  | DOLAR_IDENTIFIER { MkDolarIdentifier $1 }

int:
  | INT { $1 }

/**********************************************
 * Macros
 */

%inline
span(P):
  | P { ($1) (mkLoc $startpos, mkLoc $endpos)  }

%inline
_binoptier_once(P, OP):
  | span(P) OP span(P)
    { ($2) $1 $3 }

%inline
binoptier_assoc_right_altlast(P, OP, L):
  | span(_binoptier_assoc_right_altlast(P, OP, L))
  { $1 }

_binoptier_assoc_right_altlast(P, OP, L):
  | L { $1 }
  | span(P) OP binoptier_assoc_right_altlast(P, OP, L)
    { ($2) $1 $3 }


_binoptier_assoc_right(P, OP):
  | _binoptier_assoc_right_altlast(P, OP, P)
  { $1 }

%inline
binoptier_assoc_left(P, OP):
  | span(_binoptier_assoc_left(P, OP))
  { $1 }

_binoptier_assoc_left(P, OP):
  | P { $1 }
  | binoptier_assoc_left(P, OP) OP span(P) 
    { ($2) $1 $3 }

%inline
unoptier(P, OP):
  | span(_unoptier(P, OP))
  { $1 }

_unoptier(P, OP):
  | P { $1 }
  | OP unoptier(P, OP) { ($1) $2 }

%inline
opt(P):
    | { None }
    | P { Some $1 }

%inline
opt_alt(P, S):
    | S { None }
    | P { Some $1 }

raw_many(P):
    | 
    { [] }

    | raw_many(P) P
    { $2::$1 }

brackets(L, R, P):
  | L P R { $2 }

%inline
many(P):
    | raw_many(P)
    { List.rev $1 }

%inline
many1(P):
    | P raw_many(P)
    { $1 :: $2 }

%inline
prefix(S,P):
    | S P
    { $2 }

%inline
suffix(P,S):
    | P S
    { $1 }

%inline
pairSep(S, A, B):
  | A S B { ($1, $3) }

%inline
manySep(S,P):
    | 
    { [] }

    | manySep1(S, P)
    { $1 }

%inline
manySep1(S,P):
    | P many(prefix(S,P))
    { $1 :: $2 }
