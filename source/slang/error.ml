open Preast

type error_type =
    | ERR_ExpressionCannotUseLogicId of identifier
    | ERR_ExpressionCannotUseLogicConB of binop
    | ERR_ExpressionCannotUseLogicConU of unop
    | ERR_ExpressionCannotUseLogicGen of string
    | ERR_ExpressionCannotUseLogicQuant 
    | ERR_NotYetImplemented of string
    | ERR_ProgramIdentifierIsNotPredicate of identifier
    | ERR_ExpressionIsNotFormula
    | ERR_FormulaCannotUseExpressionOpB of binop
    | ERR_FormulaCannotUseExpressionOpU of unop
    | ERR_InvalidPredicate
    | ERR_CannotBindProgramVariables of identifier
    | ERR_ExpectedProgramIdentifier  of identifier
    | ERR_ExpectedNormalIdentifier of identifier
    | ERR_SyntaxError of string

type error = error_type * location

let string_of_error_type = function
    | ERR_ExpressionCannotUseLogicId _ ->
        "expression cannot use logic identifier"
    | ERR_ExpressionCannotUseLogicConB _ ->
        "expression cannot use logic connective"
    | ERR_ExpressionCannotUseLogicConU _ ->
        "expression cannot use logic connective"
    | ERR_ExpressionCannotUseLogicGen _ ->
        "expression cannot use logic connective"
    | ERR_ExpressionCannotUseLogicQuant ->
        "expression cannot use logic quantifiers"
    | ERR_NotYetImplemented s ->
        "not yet implemented: " ^ s
    | ERR_ProgramIdentifierIsNotPredicate _ ->
        "program identifier cannot be used as predicate name"
    | ERR_ExpressionIsNotFormula ->
        "expression is not a logic formula"
    | ERR_FormulaCannotUseExpressionOpB _ ->
        "expression connective is not a logic formula"
    | ERR_FormulaCannotUseExpressionOpU _ ->
        "expression connective is not a logic formula"
    | ERR_InvalidPredicate ->
        "invalid predicate"
    | ERR_CannotBindProgramVariables _ ->
        "program identifiers cannot be bound"
    | ERR_ExpectedProgramIdentifier _ ->
        "expected program identifier"
    | ERR_ExpectedNormalIdentifier _ ->
        "expected normal identifier"
    | ERR_SyntaxError s ->
        "syntax error: " ^ s

let string_of_error (et, loc) = 
    Printf.sprintf "%u:%u: %s" (fst loc) (snd loc) (string_of_error_type et)

let make_result_human_readable r = Lib.Result.map_error (List.map string_of_error) r

module MakeCollector () = struct

    let errors  : (error_type * location) list ref = ref []

    let add_error r = errors := (fst r, fst (snd r)) :: !errors

    let expression_cannot_use_logic_id span id = 
        add_error (ERR_ExpressionCannotUseLogicId id, span)

    let expression_cannot_use_logic_conb span binop = 
        add_error (ERR_ExpressionCannotUseLogicConB binop, span)

    let expression_cannot_use_logic_conu span unop = 
        add_error (ERR_ExpressionCannotUseLogicConU unop, span)

    let expression_cannot_use_logic_quant span = 
        add_error (ERR_ExpressionCannotUseLogicQuant, span)

    let expression_cannot_use_logic_gen span str =
        add_error (ERR_ExpressionCannotUseLogicGen str, span)

    let not_yet_implemented span s =
        add_error (ERR_NotYetImplemented s, span)

    let program_identifier_is_not_predicate span id =
        add_error (ERR_ProgramIdentifierIsNotPredicate id, span)

    let expression_is_not_formula span =
        add_error (ERR_ExpressionIsNotFormula, span)

    let formula_cannot_use_expression_opb span binop =
        add_error (ERR_FormulaCannotUseExpressionOpB binop, span)

    let formula_cannot_use_expression_opu span unop =
        add_error (ERR_FormulaCannotUseExpressionOpU unop, span)

    let invalid_predicate span =
        add_error (ERR_InvalidPredicate, span)

    let expected_normal_identifier span id = 
        add_error (ERR_ExpectedNormalIdentifier id, span)

    let expected_program_identifier span id =
        add_error (ERR_ExpectedProgramIdentifier id, span)
end
