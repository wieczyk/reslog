let debugf fmt = Lib.Log.make_debug_printf __MODULE__ fmt

module Internal = struct

    let mkLoc pos = 
        (pos.Lexing.pos_lnum, pos.Lexing.pos_cnum - pos.Lexing.pos_bol + 1)

    let open_file_lexbuf file = 
        let channel = open_in file in
        let lexbuf = Lexing.from_channel channel in
        lexbuf.Lexing.lex_curr_p <- {
            lexbuf.Lexing.lex_curr_p with
            Lexing.pos_fname = file
            };
        lexbuf

    let open_string_lexbuf buffer = 
        let lexbuf = Lexing.from_string buffer  in
        lexbuf.Lexing.lex_curr_p <- {
            lexbuf.Lexing.lex_curr_p with
            Lexing.pos_fname = "<buffer>"
            };
        lexbuf


    let parse_lexbuf _ lexbuf =
        try
            let token = Lexer.token in
            Ok (Grammar.parse_formula token lexbuf);
        with
        | Grammar.Error ->
            let loc = Lexer.mkLoc lexbuf.Lexing.lex_curr_p in 
            let src = Lexing.lexeme lexbuf in 
            let s = if String.length src > 0 
                then Printf.sprintf "unexpected token: %s" src
                else Printf.sprintf "unexpected end"
            in
            Error.(Error [ERR_SyntaxError s, loc])

        | Lexer.InvalidToken (loc, str) ->
            let s = Printf.sprintf "invalid token: %s" str in
            Error.(Error [ERR_SyntaxError s, loc])

    let parse_file f = parse_lexbuf f (open_file_lexbuf f)
    let parse_string s = parse_lexbuf "<buffer>" (open_string_lexbuf s) 

    let normalize r =
        Lib.Result.map Preast_normalizer.as_formula r

    let and_then_normalize f x =
        normalize (f x)
end

let parse_file = Internal.and_then_normalize Internal.parse_file 
let parse_string = Internal.and_then_normalize Internal.parse_string
let human_readable = Error.make_result_human_readable
