(*
 *
 *)
module PreAst = struct
  open Preast

  let pp_binop = function
    | BINOP_Add -> "+"
    | BINOP_And -> "/\\"
    | BINOP_Div -> "/"
    | BINOP_Eq -> "=="
    | BINOP_Ge -> ">="
    | BINOP_Gt -> ">"
    | BINOP_Le -> "<="
    | BINOP_Lt -> "<"
    | BINOP_Mul -> "*"
    | BINOP_Ne -> "!="
    | BINOP_Or -> "\\/"
    | BINOP_Sub -> "-"
    | BINOP_Star -> "**"
    | BINOP_ImplyStar -> "-*"
    | BINOP_Imply -> "->"

  let pp_unop = function
    | UNOP_Not -> "~"
    | UNOP_Neg-> "-"

  let pp_identifier = function
    | MkIdentifier id -> id
    | MkDolarIdentifier id -> "$" ^ id

  let rec string_of_expression = function
    | EXPR_Identifier (_, id) ->
      pp_identifier id
    | EXPR_Int (_, v) ->
      string_of_int v
    | EXPR_Call (_, callee, args) -> String.concat ""
      [ string_of_expression callee
      ; "("
      ; String.concat ", " (List.map string_of_expression args)
      ; ")"
      ]
    | EXPR_Dict (_, fs) -> 
      let pp_field (f,e) = String.concat ""
        [ pp_identifier f 
        ; ":"
        ; string_of_expression e
        ] 
      in
      String.concat ""
        [ "{" 
        ; String.concat ", "
          (List.map pp_field fs)
        ; "}"
        ]
    | EXPR_Binop (_, binop, lhs, rhs) -> String.concat ""
      [ "("
      ; string_of_expression lhs
      ; ") "
      ; pp_binop binop
      ; " ("
      ; string_of_expression rhs
      ; ")"
      ]
      
    | EXPR_Unop (_, unop, sub) -> String.concat ""
      [ pp_unop unop
      ; "("
      ; string_of_expression sub
      ; ")"
      ]
    | EXPR_Exists (_, xs, sub) -> String.concat ""
      [ "exists "
      ; String.concat " "
        (List.map pp_identifier xs)
      ; " ("
      ; string_of_expression sub
      ; ")"
      ]
    | EXPR_Forall (_, xs, sub) -> String.concat ""
      [ "forall "
      ; String.concat " "
        (List.map pp_identifier xs)
      ; " ("
      ; string_of_expression sub
      ; ")"
      ]
    | EXPR_Ptsto (_, lhs, rhs) -> String.concat " "
      [ string_of_expression lhs
      ; "|->"
      ; string_of_expression rhs
      ]
    | EXPR_Field (_, e, fld) -> String.concat " "
      [ string_of_expression e
      ; "."
      ; pp_identifier fld
      ]
end

open Ast 

let string_of_identifier = function
  | MkIdentifier s -> s
  | MkProgramIdentifier s -> "$" ^ s

let string_of_expr_binop = function
  | EXPR_BINOP_Add -> "+"
  | EXPR_BINOP_Sub -> "-"
  | EXPR_BINOP_Mul -> "*"
  | EXPR_BINOP_Div -> "/"
let string_of_expr_unop = function
  | EXPR_UNOP_Neg -> "-"
  | EXPR_UNOP_Not -> "~"

let priority_of_expr_binop = function
  | EXPR_BINOP_Add -> 2
  | EXPR_BINOP_Sub -> 2
  | EXPR_BINOP_Mul -> 1
  | EXPR_BINOP_Div -> 1

let priority_of_frml_binop = function
  | FRML_BINOP_And -> 3
  | FRML_BINOP_Star -> 3
  | FRML_BINOP_Or -> 2
  | FRML_BINOP_Imply -> 1
  | FRML_BINOP_ImplyStar -> 1

let string_of_frml_binop = function
  | FRML_BINOP_And -> "/\\"
  | FRML_BINOP_Star -> "**"
  | FRML_BINOP_Or -> "\\/"
  | FRML_BINOP_Imply -> "->"
  | FRML_BINOP_ImplyStar -> "|->"

let priority_of_expression = function
  | EXPR_Binop (b, _, _) -> priority_of_expr_binop b
  | _ -> 0

let priority_of_formula = function
  | FRML_Binop (b, _, _) -> priority_of_frml_binop b
  | _ -> 0

let brackets a b e =
  if a > b then 
    "(" ^ e ^ ")"
  else
    e

let rec string_of_expression = function
  | EXPR_Identifier x -> string_of_identifier x
  | EXPR_Int i -> string_of_int i
  | EXPR_Binop (b, lhs, rhs) ->
    let lhs_prio = succ (priority_of_expression lhs) in
    let rhs_prio = priority_of_expression rhs in
    let my_prio = priority_of_expr_binop b in
    let lhs = string_of_expression lhs in 
    let lhs = brackets lhs_prio my_prio lhs in
    let rhs = string_of_expression rhs in
    let rhs = brackets rhs_prio my_prio rhs in 
    String.concat " "
      [ lhs
      ; string_of_expr_binop b
      ; rhs
      ]
  | EXPR_Unop (u, sub) ->
    let sub_prio = priority_of_expression sub in 
    let sub = string_of_expression sub in
    let sub = brackets sub_prio 0 sub in
    String.concat " "
      [ string_of_expr_unop u
      ; sub
      ]

  | EXPR_Call _ -> "call"
  | EXPR_Dict flds -> String.concat ""
    [ "{"
    ; String.concat ", " (List.map (fun (fld, e) -> fld ^ ": " ^ string_of_expression e) flds)
    ; "}"
    ]
  | EXPR_Nil -> "nil"
  | EXPR_Field (e, fld) -> String.concat ""
    [ string_of_expression e
    ; "."
    ; string_of_identifier fld
    ]

let rec string_of_formula = function
  | FRML_True -> "True"
  | FRML_False -> "False"
  | FRML_Empty -> "Empty"
  | FRML_Ptsto (lhs, rhs) -> String.concat " "
    [ string_of_expression lhs
    ; "|->"
    ; string_of_expression rhs
    ]
  | FRML_Binop (b, lhs, rhs) ->
    let lhs_prio = succ (priority_of_formula lhs) in
    let rhs_prio = priority_of_formula rhs in
    let my_prio = priority_of_frml_binop b in
    let lhs = string_of_formula lhs in 
    let lhs = brackets lhs_prio my_prio lhs in
    let rhs = string_of_formula rhs in
    let rhs = brackets rhs_prio my_prio rhs in 
    String.concat " "
      [ lhs
      ; string_of_frml_binop b
      ; rhs
      ]
  | FRML_Predicate (id, exprs) ->
    let id = string_of_identifier id in
    let exprs = List.map string_of_expression exprs in
    let exprs = match exprs with
      | [] -> ""
      | _ -> String.concat "" [ "("; String.concat ", " exprs; ")"]
    in
    String.concat ""
      [ id
      ; exprs
      ]
  | _ -> "(printer cannot print it)"

let pp_formula = string_of_formula
