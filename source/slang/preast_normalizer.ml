open Ast
let debugf fmt = Lib.Log.make_debug_printf __MODULE__ fmt

module Pre = Preast

module type Assumptions = sig
  (* currently empty *)
end

module Implementation(M: Assumptions) = struct
  module Error = Error.MakeCollector ()

  let dummy_expression = EXPR_Int 0

  let dummy_identifier = MkIdentifier "?"
  let dummy_program_identifier = MkProgramIdentifier "?"

  let as_identifier = function
    | Pre.MkIdentifier i -> MkIdentifier i
    | Pre.MkDolarIdentifier i -> MkProgramIdentifier i

  let is_normal_identifier = function
    | Pre.MkIdentifier _ -> true
    | Pre.MkDolarIdentifier _ -> false

  let is_dolar_identifier = function
    | Pre.MkIdentifier _ -> false
    | Pre.MkDolarIdentifier _ -> true

  let is_expression_binop = function
    | Pre.BINOP_Add
    | Pre.BINOP_Sub
    | Pre.BINOP_Mul
    | Pre.BINOP_Div ->
      true
    | _ -> 
      false

  let is_relation_binop = function
  | Pre.BINOP_Eq
  | Pre.BINOP_Ne
  | Pre.BINOP_Lt
  | Pre.BINOP_Gt
  | Pre.BINOP_Le
  | Pre.BINOP_Ge ->
    true
  | _ ->
    false
  
  let as_relation_binop = function
    | Pre.BINOP_Eq -> REL_BINOP_Eq
    | Pre.BINOP_Ne -> REL_BINOP_Ne
    | Pre.BINOP_Lt -> REL_BINOP_Lt
    | Pre.BINOP_Gt -> REL_BINOP_Gt
    | Pre.BINOP_Le -> REL_BINOP_Le
    | Pre.BINOP_Ge -> REL_BINOP_Ge 
    | _ ->
      failwith "Impossible, expected relational expression"

  let is_formula_binop binop = not (is_expression_binop binop || is_relation_binop binop)

  let is_expression_unop = function
    | Pre.UNOP_Neg ->
      true
    | _ ->
      false

  let is_formula_unop unop = not (is_expression_unop unop)

  let as_expression_binop = function
    | Pre.BINOP_Add -> EXPR_BINOP_Add
    | Pre.BINOP_Mul -> EXPR_BINOP_Mul
    | Pre.BINOP_Sub -> EXPR_BINOP_Sub
    | Pre.BINOP_Div -> EXPR_BINOP_Div
    | _ -> 
      failwith "Impossible, using logic connective as term expression"

    
  let as_formula_binop = function
    | Pre.BINOP_And       -> FRML_BINOP_And
    | Pre.BINOP_Or        -> FRML_BINOP_Or
    | Pre.BINOP_Imply     -> FRML_BINOP_Imply
    | Pre.BINOP_ImplyStar -> FRML_BINOP_ImplyStar
    | Pre.BINOP_Star      -> FRML_BINOP_Star
    | _ -> 
      failwith "Impossible, using operator as logic connective"

  let as_expression_unop = function
    | Pre.UNOP_Neg -> EXPR_UNOP_Neg
    | _ ->
      failwith "Impossible, using logic connective as term expression"

  let as_formula_unop = function
    | Pre.UNOP_Not -> FRML_UNOP_Not
    | _ ->
      failwith "Impossible, using logic connective as term expression"

  let string_of_identifier = function
    | Pre.MkIdentifier x -> x
    | Pre.MkDolarIdentifier x -> "$" ^ x

  (*
   * Translatting parsed expression as term
   *)
  let rec as_expression: Pre.span_location Pre.expression -> expression = function
    | Pre.EXPR_Identifier (_, id) when string_of_identifier id = "null" ->
      EXPR_Nil

    | Pre.EXPR_Identifier (_, id) ->
        EXPR_Identifier (as_identifier id)

    | Pre.EXPR_Int (_, v) ->
      EXPR_Int v

    | Pre.EXPR_Binop (span, binop, lhs, rhs) ->
      if is_expression_binop binop then
        let binop = as_expression_binop binop in 
        let lhs = as_expression lhs in
        let rhs = as_expression rhs in
        EXPR_Binop (binop, lhs, rhs)
      else
        (Error.expression_cannot_use_logic_conb span binop; dummy_expression)

    | Pre.EXPR_Unop (span, unop, sub) -> 
      if is_expression_unop unop then
        let unop = as_expression_unop unop in 
        let sub = as_expression sub in 
        EXPR_Unop (unop, sub)
      else
        (Error.expression_cannot_use_logic_conu span unop; dummy_expression)

    | Pre.EXPR_Call (_, callee, args) -> 
      let callee = as_expression callee in 
      let args = List.map as_expression args in 
      EXPR_Call (callee, args)

    | Pre.EXPR_Dict (_, flds) ->
      let map_field (fld, expr) = 
        (string_of_identifier fld, as_expression expr)
      in
      let flds = List.map map_field flds in 
      EXPR_Dict flds

    | Pre.EXPR_Exists (span, _, _) 
    | Pre.EXPR_Forall (span, _, _) -> 
      Error.expression_cannot_use_logic_quant span; dummy_expression

    | Pre.EXPR_Ptsto (span, _, _) ->
      Error.expression_cannot_use_logic_gen span "ptsto"; dummy_expression

    | Pre.EXPR_Field (_, e, fld) ->
      let e = as_expression e in
      let fld = as_identifier fld in
      EXPR_Field (e, fld)


    let dummy_formula = FRML_True

    (*
     * Translatting parsed expression as separation logic formula
     *)

    let rec as_formula: Pre.span_location Pre.expression -> formula = function
    | Pre.EXPR_Identifier (_, Pre.MkIdentifier "True") ->
      FRML_True

    | Pre.EXPR_Identifier (_, Pre.MkIdentifier "Empty") ->
      FRML_Empty

    | Pre.EXPR_Identifier (_, Pre.MkIdentifier "False") ->
      FRML_False

    | Pre.EXPR_Identifier (_, id) when is_normal_identifier id -> 
      FRML_Predicate (as_identifier id, [])

    | Pre.EXPR_Identifier (span, _) -> 
      Error.invalid_predicate span;
      dummy_formula

    | Pre.EXPR_Int (span, _) ->
      Error.expression_is_not_formula span; dummy_formula

    | Pre.EXPR_Binop (span, binop, lhs, rhs) ->
      if is_relation_binop binop then
        let binop = as_relation_binop binop in 
        let lhs = as_expression lhs in
        let rhs = as_expression rhs in
        FRML_Relation (binop, lhs, rhs)
      else if is_formula_binop binop then
        let binop = as_formula_binop binop in 
        let lhs = as_formula lhs in
        let rhs = as_formula rhs in
        FRML_Binop (binop, lhs, rhs)
      else
        (Error.formula_cannot_use_expression_opb span binop; dummy_formula)

    | Pre.EXPR_Unop (span, unop, sub) -> 
      if is_formula_unop unop then
        let unop = as_formula_unop unop in 
        let sub = as_formula sub in 
        FRML_Unop (unop, sub)
      else
        (Error.formula_cannot_use_expression_opu span unop; dummy_formula)

    | Pre.EXPR_Call (_, Pre.EXPR_Identifier (_, id) , args) when is_normal_identifier id -> 
      let id = as_identifier id in
      let args = List.map as_expression args in 
      FRML_Predicate (id, args)

    | Pre.EXPR_Call (span, _, _) -> 
      Error.invalid_predicate span; dummy_formula

    | Pre.EXPR_Dict (span, _) ->
      Error.not_yet_implemented span "dict"; dummy_formula

    | Pre.EXPR_Exists (_, xs, sub)  ->
      let sub = as_formula sub in 
      let xs = List.map as_identifier xs in 
      FRML_Exists (xs, sub)

    | Pre.EXPR_Forall (_, xs, sub) -> 
      let sub = as_formula sub in 
      let xs = List.map as_identifier xs in 
      FRML_Forall (xs, sub)

    | Pre.EXPR_Ptsto (_, lhs, rhs) ->
      let lhs = as_expression lhs in
      let rhs = as_expression rhs in 
      FRML_Ptsto (lhs, rhs)

    | Pre.EXPR_Field (span, _, _) ->
      Error.invalid_predicate span; dummy_formula

  let check_errors f x =
    let y = f x in 
    if !Error.errors == [] then
      Ok y
    else
      Error !Error.errors

end

let as_expression x =
  debugf "Normalizing as expression: %s" (Printer.PreAst.string_of_expression x);
  let module Assumptions = struct end in 
  let module M = Implementation(Assumptions) in 
  M.check_errors M.as_expression x

let as_formula x =
  debugf "Normalizing as formula: %s" (Printer.PreAst.string_of_expression x);
  let module Assumptions = struct end in 
  let module M = Implementation(Assumptions) in 
  let y = M.check_errors M.as_formula x in
  begin match y with
  | Ok y -> debugf "Normalized: %s" (Printer.pp_formula y)
  | Error _ -> debugf "Failed"
  end;
  y
