

let make_debugf prefix fmt = 
  let cont s = Printf.printf "%30s: %s\n%!" prefix s in
  Printf.ksprintf cont fmt