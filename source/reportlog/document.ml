

type entry_element =
  | Text of string
  | Code of string

type entry = entry_element list

type document_element =
  | StartSection of string * entry list
  | EndSection
