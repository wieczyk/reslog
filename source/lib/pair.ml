module type S = sig
  type left
  type right

  type t = left * right
end

module MakeComparablePair(L:Map.OrderedType)(R:Map.OrderedType) = struct
  type left = L.t
  type right = R.t
  type t = left * right

  let compare = Utils.compare_pair L.compare R.compare
end

