
let debugf fmt = Log.make_debug_printf __MODULE__ fmt

module type SType = sig
  type t

  val hash: t -> int

  val equal: t -> t -> bool
end

module type S = sig

  type elt

  type q

  val abstract: elt -> q

  val canonical: q -> elt

  module Handle: Unique.SHandle with type t = q

  module Table: Hashtbl.S with type key = q

  module Map: Map.S with type key = q

  module Set: Set.S with type elt = q
end

module MakeQuotient(M:SType): S with type elt = M.t = struct

  type elt = M.t

  module Handle = Unique.MakeHandle(M)

  type q = Handle.t

  module InternalTable = Hashtbl.Make(M)

  module Table = Unique.MakeHashtblFromHandle(Handle)
  module Set = Set.Make(Handle)
  module Map = Map.Make(Handle)

  let internal_table = InternalTable.create 513

  let abstract x =
    try
      debugf "Trying to find node for hash=%u" (M.hash x);
      let out = InternalTable.find internal_table x in
      debugf "found %u" (Handle.unpack_index out);
      out
    with Not_found ->
      debugf "Not found, creating new";
      let q = Handle.make x in
      InternalTable.replace internal_table x q;
      debugf "created %u" (Handle.unpack_index q);
      q

  let canonical x = Handle.unpack x

end