module type S = sig

  type t

  type elt

  type handle

  val make: unit -> t

  val insert: t -> elt -> handle

  val unpack: t -> handle -> elt

  val find_handle: t -> handle -> handle

  val find: t -> elt -> elt

  val union_handle: t -> handle -> handle -> unit

  val union: t -> elt -> elt -> unit

  val are_unified: t -> elt -> elt -> bool

  val are_unified_handle: t -> handle -> handle -> bool

  val canonicals: t -> elt list

end

module Raw = struct

  type 'a elt =
    { mutable parent : int
    ; mutable elem   : 'a option
    ; mutable rank   : int
    }

  let elt0 () =
    { parent = 0
    ; rank   = 0
    ; elem   = None
    }

  type 'a t =
    { mutable elements: 'a elt array
    ; mutable size: int
    }

  let make () =
    { elements = Array.make 21 (elt0 ())
    ; size = 0
    }

  let rec make_new_handle t elem =
    if t.size < Array.length t.elements then
      let handle = t.size in
      t.size <- t.size + 1;
      let elt = 
        { parent = handle
        ; elem = Some elem
        ; rank = 0
        } in
      Array.set t.elements handle elt;
      handle
    else
      let new_half = Array.make (Array.length t.elements) (elt0 ()) in
      t.elements <- Array.append t.elements new_half;
      make_new_handle t elem

  let rec find_handle t handle =
    let descr = (Array.get t.elements handle) in
    let parent = descr.parent in
    if handle = parent then
      handle
    else
      let y = find_handle t parent in
      descr.parent <- y;
      y

  let union_handle t lhs rhs = 
    let lhs = find_handle t lhs in
    let rhs = find_handle t rhs in

    if lhs != rhs then begin
      let lhs_elem = Array.get t.elements lhs in 
      let rhs_elem = Array.get t.elements rhs in 
      if lhs_elem.rank <= rhs_elem.rank then
        (lhs_elem.parent <- rhs; lhs_elem.rank <- lhs_elem.rank + 1)
      else 
        (rhs_elem.parent <- lhs; rhs_elem.rank <- rhs_elem.rank + 1)
    end

  let are_unified_handle t lhs rhs = 
    let lhs = find_handle t lhs in
    let rhs = find_handle t rhs in
    lhs = rhs

  let unpack t handle = 
    match (Array.get t.elements handle).elem with
    | Some a -> a
    | None -> failwith "union find internal error: derefencing invalid handle"


  let canonicals_elems t =
    let rec loop aux = function
      | i when i == t.size -> aux
      | i ->
        let descr = Array.get t.elements i in
        let aux = if descr.parent = i then
            Utils.from_some descr.elem :: aux
          else
            aux
        in
        loop aux (succ i)
    in
      loop [] 0


end


module MakeHashing(H:Hashtbl.HashedType) = struct

  module Table = Hashtbl.Make(H)

  type t = 
    { mapping: int Table.t
    ; raw: H.t Raw.t
    }

  type handle = int

  type elt = H.t

  let make () = 
    { mapping= Table.create 513
    ; raw = Raw.make ()
    }

  let insert t el =
    try
      Table.find t.mapping el 
    with Not_found ->
      let h = Raw.make_new_handle t.raw el in
      Table.replace t.mapping el h;
      h

  let unpack t = Raw.unpack t.raw 

  let find_handle t h = Raw.find_handle t.raw h

  let union_handle t = Raw.union_handle t.raw

  let are_unified_handle t = Raw.are_unified_handle t.raw

  let are_unified t a b = are_unified_handle t (insert t a) (insert t b)

  let union t a b = union_handle t (insert t a) (insert t b)

  let find t a = unpack t (find_handle t (insert t a))

  let canonicals t = Raw.canonicals_elems t.raw

end