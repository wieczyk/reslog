let debugf fmt = Log.make_debug_printf __MODULE__ fmt

module Normal = struct

  type ('a, 'b, 'c) step_result =
    | Final of ('a, 'b) Result.t
    | Or of 'c list
    | And of 'c list * ('a list -> ('a, 'b) Result.t)

  let rec search compute cont task = 
    match compute task with
    | Final v -> cont v
    | Or tasks ->
      search_first compute cont tasks
    | And (tasks, merge) ->
      search_all compute cont merge [] tasks

  and search_first compute cont = function
    | [task] ->
      search compute cont task
    | task :: tasks ->
      let subcont = function
        | Error _ ->
          search_first compute cont tasks
        | v ->
          cont v
      in
      search compute subcont task
    | [] -> 
      failwith "BacktrackingFramework.search_first called on empty list"

  and search_all compute cont merge acc = function
  | [] ->
    let acc = List.rev acc in
    cont (merge acc)
  | task :: tasks -> 
    let subcont = function
      | Ok a -> 
        search_all compute cont merge (a::acc) tasks
      | v ->
        cont v
    in
    search compute subcont task

  let compute f task =
    let cont x = x in
    search f cont task

end

module MakeRuleBased(M: sig
  type goal

  type side

  val goal_to_string: goal -> string

  val side_to_string: side -> string

  end) = struct

  open M

  type step_result =
    | Goals of goal list
    | Collect of side
    | Failure

  type system = string * (goal -> step_result)

  let first_rule rs: system =
    let f g = 
      let rec loop = function
        | [] ->
          Failure
        | (name, r) :: rs ->
          if String.length name > 0 then debugf "trying rule %s" name;
          begin match r g with
          | Failure ->
            loop rs
          | y -> 
            y
          end
      in
      loop rs
    in
      ("", f)

  let rec fix tag ((_, f) as  system) imms ggs = function
    | [] ->
      begin match ggs with
      | gs :: ggs ->
        fix tag system imms ggs gs
      | [] ->
        Some imms
      end
    | (goal_tag, g) :: gs ->  
    debugf "Executing: goal %u: %s" goal_tag (goal_to_string g);
    match f g with
    | Failure ->
      debugf "Result: goal %u: %s: failure" goal_tag (goal_to_string g);
      None
    | Collect x ->
      debugf "Result: goal %u: %s: success; collect: %s" goal_tag (goal_to_string g) (side_to_string x);
      fix tag system (x :: imms) ggs gs
    | Goals ngs ->
      debugf "Result: goal %u: %s: new %u goals: " goal_tag (goal_to_string g) (List.length ngs);
      let fold_goal (tag, ngs) g = (succ tag, (tag, g) :: ngs) in
      let tag, ngs = List.fold_left fold_goal (tag, []) ngs in
      let tags = List.map fst ngs in 
      let tags = List.map string_of_int tags in
      let tags = String.concat " " tags in 
      debugf "      \\ new goals: %s" tags;
      fix tag system imms (ngs::ggs) gs

  let search system g = fix 1 system [] [] [(0, g)]

end
