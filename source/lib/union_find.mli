module type S = sig

  type t

  type elt

  type handle

  val make: unit -> t

  val insert: t -> elt -> handle

  val unpack: t -> handle -> elt

  val find_handle: t -> handle -> handle

  val find: t -> elt -> elt

  val union_handle: t -> handle -> handle -> unit

  val union: t -> elt -> elt -> unit

  val are_unified: t -> elt -> elt -> bool

  val are_unified_handle: t -> handle -> handle -> bool

  val canonicals: t -> elt list

end


module MakeHashing: functor(H:Hashtbl.HashedType) -> S with type elt = H.t
