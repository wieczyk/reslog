

type ('a, 'b) t = ('a, 'b) result

let map f = function
  | Ok x -> f x
  | Error x -> Error x

let map_error f = function
  | Error x -> Error (f x)
  | Ok x -> Ok x

let is_ok = function
  | Ok _ -> true
  | _ -> false

let is_error x = not (is_ok x)

let get = function
  | Ok x -> x
  | _ -> failwith "Result.get called on error-value"

let error = function
  | Error x -> x
  | _ -> failwith "Result.error called on proper-value"
