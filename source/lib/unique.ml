
module type S = sig
  type t
end

module type SIndex = sig
  type t

  val make: unit-> t
  val hash: t -> int
  val compare: t -> t -> int
  val equal: t -> t -> bool
  val unpack: t -> int
end

module MakeIndex () = struct

  type t = int

  let counter : t ref = ref 0

  let make () : t = incr counter; !counter

  let hash (t:t) = Hashtbl.hash t

  let compare (a:t) (b:t) = compare a b

  let equal (a:t) (b:t) = a = b

  let unpack (t:t) = t

end 

module type SHandle = sig

  module Index : SIndex

  type data
  type t 

  val make: data -> t
  val hash: t -> int
  val compare: t -> t -> int
  val equal: t -> t -> bool
  val unpack: t -> data
  val unpack_index: t -> int
end

module MakeHashtblFromIndex(I:SIndex) = Hashtbl.Make(I)
module MakeHashtblFromHandle(H:SHandle) = Hashtbl.Make(H)

module MakeHandle(M:S) = struct
  module Index = MakeIndex ()

  type data = M.t
  type t = Index.t * M.t

  let make x = (Index.make (), x)
  let unpack (_, t) = t
  let hash (i, _) = Index.hash i
  let index (i, _) = i

  let compare a b = Index.compare (index a) (index b)
  let equal a b = compare a b == 0

  let unpack_index x = Index.unpack (index x)


end
