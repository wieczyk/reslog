
(**
 Internal module for dumping resource as files
 *)
module ResourceManager = struct
    let directory = ref ""

    let initialize dir =
        let dir = String.escaped dir in
        if false || Sys.file_exists dir then 
            ignore (Sys.command (Printf.sprintf "rm -rf \"%s\"" dir));
        ignore (Sys.command (Printf.sprintf "mkdir \"%s\"" dir));
        directory := dir

    let reserve_name name =
        let name = String.escaped name in 
        Printf.sprintf "%s/%s" !directory name 

    let open_file name =
        open_out (reserve_name name)

    let dump_resource name f =
        let channel = open_file name in
        let buffer = f () in
        output_string channel buffer;
        if String.length(buffer) = 0 || String.get buffer (String.length buffer - 1) != '\n' then
            output_string channel "\n";
        close_out channel

end

(** Various counters aggergated in own module *)
module Phases = struct

    (** Phase counter, increased by named steps in pipeline *)
    let counter = ref 0

    (** Resource counter, increased with each resource dump *)
    let resource_counter = ref 0

    (** Phase name *)
    let name  = ref "init"

end

let _ = ResourceManager.initialize "log"

let debug2_enabled = ref false

let debug_printf fmt = 
    let cont = Printf.printf "%s\n%!"  in
    Printf.ksprintf cont fmt

let debug2_printf fmt = 
    let cont s = 
        if !debug2_enabled then
            Printf.printf "%s\n%!" s
    in
    Printf.ksprintf cont fmt

let tty_make_debug_printf prefix =
    let cont s = debug_printf "\o033[1m%30s:\o033[0m %s" prefix s in
    Printf.ksprintf cont 

let txt_make_debug_printf prefix =
    let cont s = debug_printf "%30s: %s" prefix s in
    Printf.ksprintf cont 

let make_debug_printf =
    if Unix.isatty Unix.stdout then 
        tty_make_debug_printf
    else
        txt_make_debug_printf

let make_debug2_printf =
    if Unix.isatty Unix.stdout then 
        tty_make_debug_printf
    else
        txt_make_debug_printf

let dump_resource name f =
    let name = Printf.sprintf "%u.%s.%u.%s" 
        !Phases.counter
        !Phases.name
        !Phases.resource_counter 
        name
    in
    incr Phases.resource_counter;
    make_debug_printf __MODULE__ "dumping resource %s" name;
    ResourceManager.dump_resource name f

(** Informs log system that pipeline phase has been changed *)
let new_phase s = 
    Phases.name := s;
    incr Phases.counter;
    Phases.resource_counter := 0
