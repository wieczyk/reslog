(** Logging system *)

(** Dumps named resource. *)
val dump_resource: string -> (unit -> string) -> unit

(** Informs log system that pipeline phase has been changed *)
val new_phase: string -> unit

(** Printf derivative for logs *)
val debug_printf: ('a, unit, string, unit) format4 -> 'a
val debug2_printf: ('a, unit, string, unit) format4 -> 'a

(** Printf derivative for logs, is parametrized by prefix.
    Parameterized by __MODULE__ to create module-specific debug logs *)
val make_debug_printf: string -> ('a, unit, string, unit) format4 -> 'a
val make_debug2_printf: string -> ('a, unit, string, unit) format4 -> 'a