

module type S = sig
  type t
end

module type SIndex = sig
  type t

  val make: unit-> t
  val hash: t -> int
  val compare: t -> t -> int
  val equal: t -> t -> bool
  val unpack: t -> int
end

module type SHandle = sig
  module Index : SIndex

  type data
  type t 

  val make: data -> t
  val hash: t -> int
  val compare: t -> t -> int
  val equal: t -> t -> bool
  val unpack: t -> data
  val unpack_index: t -> int
end


module MakeIndex (): SIndex
module MakeHashtblFromIndex(I:SIndex) : Hashtbl.S with type key = I.t
module MakeHashtblFromHandle(H:SHandle) : Hashtbl.S with type key = H.t
module MakeHandle(M:S) : SHandle with type data = M.t