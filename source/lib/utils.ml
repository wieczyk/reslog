type location = int * int
type span_location = location * location

let adjust_location (a_col, a_ln) (b_col, b_ln) =
  if b_col == 1 then
    (a_col, a_ln + b_ln)
  else
    (a_col + b_col - 1, b_ln)

let adjust_span_location (a_begin, a_end) (b_begin, b_end) =
    (adjust_location a_begin b_begin, adjust_location a_end b_end)


let to_string_s g k x = String.concat k (g x)


module StringOps = struct

  type t = string
  let compare: t -> t -> int = compare

  let hash: t -> int = Hashtbl.hash

  let equal: t -> t -> bool = (=)

end

module StringMap = Map.Make(StringOps)
module StringSet = Set.Make(StringOps)
module StringTable = Hashtbl.Make(StringOps)

module IntOps = struct

  type t = int
  let compare: t -> t -> int = compare

  let hash: t -> int = Hashtbl.hash

  let equal: t -> t -> bool = (=)

end

module IntMap = Map.Make(IntOps)
module IntSet = Set.Make(IntOps)
module IntTable = Hashtbl.Make(IntOps)

let compare_pair compare_a compare_b (a0, b0) (a1, b1) =
  let cmp0 = compare_a a0 a1 in
  if cmp0 == 0 then
    compare_b b0 b1
  else
    cmp0

let compare_triple compare_a compare_b compare_c (a0, b0, c0) (a1, b1, c1) =
  compare_pair
    compare_a
    (compare_pair compare_b compare_c)
    (a0, (b0, c0)) (a1, (b1, c1))
    
let rec list_compare compare xs ys = match xs, ys with
  | x :: xs, y :: ys ->
    let res = compare x y in
    if res == 0
      then list_compare compare xs ys
      else res
  | [], [] -> 0
  | _ :: _, [] -> 1
  | [], _ :: _ -> -1

let is_some = function
  | None -> false
  | Some _ -> true


let rec list_mapopt f = function
  | [] -> []
  | x :: xs -> 
    begin match f x with
    | Some y -> y :: list_mapopt f xs
    | None -> list_mapopt f xs
    end


let rec insert_sorted compare x = function
  | y :: ys when compare y x < 0 -> y :: insert_sorted compare x ys
  | ys -> x :: ys

let rec insert_sorted_uniq compare x = function
  | y :: ys ->
    begin match compare y x with
    | k when k < 0 -> y :: insert_sorted_uniq compare x ys
    | k when k == 0 -> y :: ys
    | _ -> x :: y :: ys
    end
  | ys -> x :: ys

let ordered_pair compare a b =
  if compare a b <= 0 then
    (a,b)
  else
    (b,a)

let from_some = function
  | Some a -> a
  | _ -> failwith "Utils.from_some called on None"

let map_opt f = function
  | Some a -> Some (f a)
  | None -> None

let bind_opt f = function
  | Some a -> f a
  | None -> None
